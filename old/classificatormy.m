load('Color1.txt');
Color1=Color1';%due to easiness in file creation
y=Color1(1,:);%labels
x=Color1(2:end,:);%data

%%ununderstood parameters
labels               = unique(y);

options.method       = 7;
options.holding.rho  = 0.7;
options.holding.K    = 242;

options.weaklearner  = 0; %0->stump, 1->perceptron
options.epsi         = 0.1;%%perceptron
options.lambda       = 1e-2;%%perceptron
options.max_ite      = 1000;%%perceptron
options.T            = 50;%number of weak lerners

%%training itself
model_gentle=gentleboost_model(x,y,options.T,options);

rgb=imread('rodinka6.bmp');

R = im2double(rgb (:,:,1));
G = im2double(rgb (:,:,2));
B = im2double(rgb (:,:,3));
I=rgb2gray(rgb); 
I = imadjust(I);
%figure, imshow(I)
BWi = im2bw(I,0.3);
%figure, imshow(1-BWi)

Centr = bwselect(1-BWi);
%figure, imshow(Centr)
kraybl = bwselect(1-BWi);
%figure, imshow(kraybl)
S = sum(Centr(:));

%bw2dots = or (Centr, kraybl);

stat  = regionprops(Centr, 'centroid');
   centrCentr = cat(1, stat.Centroid);
      
stat  = regionprops(kraybl, 'centroid');
   centrKraybl = cat(1, stat.Centroid);
   
BWdots = im2bw(I,0.3);
BWdots = 1-BWdots;
%figure, imshow(BWdots)

Cx = centrCentr(1);
Cy = centrCentr(2);

Kx = centrKraybl(1);
Ky = centrKraybl(2);

Vx = Kx-Cx;
Vy = Ky-Cy;

bluex = Cx+Vy;
bluey = Cy-Vx;

blue = bwselect(BWdots,bluex,bluey);
%figure, imshow(blue)

redx = Cx-Vy;
redy = Cy+Vx;

red = bwselect(BWdots,redx,redy);
%figure, imshow(red)

greenx = Cx-Vx;
greeny = Cy-Vy;

green = bwselect(BWdots,greenx,greeny);
%figure, imshow(green)


BW = 1-im2bw(I,0.3);
%imshow(BW)

rodinka = bwselect(BW);
%figure, imshow(rodinka)

Nr=mean(R(rodinka(:)));
Ng=mean(G(rodinka(:)));
Nb=mean(B(rodinka(:)));

Rr=mean(R(red(:)))+eps;
Rg=mean(G(red(:)))+eps;
Rb=mean(B(red(:)))+eps;

Gr=mean(R(green(:)))+eps;
Gg=mean(G(green(:)))+eps;
Gb=mean(B(green(:)))+eps;

Br=mean(R(blue(:)))+eps;
Bg=mean(G(blue(:)))+eps;
Bb=mean(B(blue(:)))+eps;

vector=[Nr/Rr,Ng/Rg,Nb/Rb,Nr/Gr,Ng/Gg,Nb/Gb,Nr/Br,Ng/Bg,Nb/Bb];
vector=vector';

%%classify
[yTest,fxtrain]=gentleboost_predict(vector,model_gentle,options);

yTest=yTest+1;

switch yTest
    case yTest==1
        disp('1')
    case yTest==2
        disp('2')
    case yTest==3
        disp('3')
    case yTest==4
        disp('4')
    case yTest==5
        disp('5')
    case yTest==6
        disp('6')
    case yTest==7
        disp('7')
    case yTest==8
        disp('8')
    case yTest==9
        disp('9')
    case yTest==10
        disp('10')
    case yTest==11
        disp('11')
    case yTest==12
        disp('12')
    case yTest==13
        disp('1')
    case yTest==14
        disp('14')
    case yTest==15
        disp('15')
    case yTest==16
        disp('16')
    case yTest==17
        disp('17')
    case yTest==18
        disp('1')
    case yTest==19
        disp('19')
    case yTest==20
        disp('20')
    case yTest==21
        disp('21')
    case yTest==22
        disp('22')
    case yTest==23
        disp('23')
    case yTest==24
        disp('24') 
    case yTest==25
        disp('Черный')
    case yTest==26
        disp('26')
end;
