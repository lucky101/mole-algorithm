%%create_TSet.m
%creates the training set on the basis of the present images
%using the create_vector function to create the characteristic vector
%in order to create a training set that could be used for
%multi-class adaboost
%everything will be saved in a text file under the format:
%[label, d_cyto, Area, Area/perimeter, #min, exc, Ener, Corr, Cont];
%the file is saved in lines, hences requires transposition before training

%labels:
%1 2 3 4
%E L M N

clear all
close all

train=4;%define the label

%%load images
MatName=ls('*.bmp');
[row,col]=size(MatName); %the number of files

%%open saving file
id=fopen('Leuko_train.txt','a');

for n=1:row
%%run over all the images

    %get image
    name=strcat(MatName(n,:));
    X=imread(name);
    Y=rgb2hsv(X);

    %create vector
    vector=create_vector(Y(:,:,2),train);
    
    %write to file
    fprintf(id,'%f %f %f %f %f %f %f %f %f\r\n',vector);
    
end

%%close saving file
fclose(id)

%%rainbow signal that it is time to quit reading comics and interpret results
t=0:0.001:10;
x1=0.4*(-t.^2+10*t);
x2=0.6*(-t.^2+10*t);
x3=0.8*(-t.^2+10*t);
x4=1.0*(-t.^2+10*t);
x5=1.2*(-t.^2+10*t);
x6=1.4*(-t.^2+10*t);
x7=1.6*(-t.^2+10*t);
% plot(t,x1,'m'), hold on
% plot(t,x2,'b')
% plot(t,x3,'c')
% plot(t,x4,'g')
% plot(t,x5,'y')
% plot(t,x6,'r')
% plot(t,x7,'k')
curve=sin(2*pi*12*x1)+sin(2*pi*12*x2)+sin(2*pi*12*x3)+sin(2*pi*12*x4)+sin(2*pi*12*x5)+sin(2*pi*12*x6)+sin(2*pi*12*x7);
curve=curve/max(curve);
player=audioplayer(curve,8192);
play(player)