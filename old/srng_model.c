
/*

  Supervised Relevance Natural Gaz classification algorithm.

  Usage
  ------

  [Wproto_est , yproto_est , E_SRNG] = srng_model(Xtrain , ytrain , [Wproto] , [yproto] , [lambda] , [options]);

  
  Inputs
  -------

  Xtrain                                Train data (d x Ntrain)
  ytrain                                Train label (1 x Ntrain) with m different classes
  Wproto                                Initial prototypes weights (d x Nproto) (default Wproto (d x Nproto) where Nproto=round(sqrt(Ntrain)). 
                                        Wproto ~ N(m_{ytrain=i},sigma_{ytrain=i}), where m_{ytrain=i} = E[Xtrain|ytrain=i], sigma_{ytrain=i} = E[XtrainXtrain'|ytrain=i]
  yproto                                Intial prototypes label (1 x Nproto) where card(yproto)=card(ytrain)
  lambda                                Weigths factor  (d x 1). Default lambda = (1/d)*ones(d , 1);

  options                               Options'structure
         .epsilonk                      Update weight's for class label between prototype and train data (default = 0.01).
         .epsilonl                      Update weight's for class label between prototype and train data (default = 0.01).
         .epsilonlambda                 lambda rate (default 10e-6)  
          sigmastart                    Start sigma size neigbhourg (default 2)
         .sigmaend                      Final sigma size
         .sigmastretch                  Sigma step stretch;
         .threshold                     Threshold to speed up computation (default 10e-10);
 		 .xi                            Constant in the sigmoid function  (default = 1)
         .nb_iterations                 Number of iterations (default = 1000)
         .metric_method                 Euclidean 2 or Euclidean4l1 (default = 1)
		 .shuffle                       Randomly permute order of train data between each iteration if shuffle=1 (default = 1)
		 .updatelambda                  Update Lambda if = 1 (default = 1)

  
  Outputs
  -------
  
  Wproto_est                            Final prototypes weigths (d x Nproto)
  yproto_est                            Final prototypes labels  (1 x Nproto)
  lambda_est                            Final Weigths factor  estimated (d x 1)
  E_SRNG                                Energy criteria versur iteration (1 x options.nb_iterations)


  To compile
  ----------


  mex   -DranSHR3 -output srng_model.dll srng_model.c

  mex   -DranSHR3 -f mexopts_intel10.bat -output srng_model.dll srng_model.c



  Example 1
  ---------

  d                                     = 2;
  Ntrain                                = 100;
  m                                     = 2;
  M0                                    = [0 ; 0];
  R0                                    = [1 0 ; 0 1];
  M1                                    = [2 ; 3];
  R1                                    = [0.5 0.1 ; 0.2 1];
  vect_test                             = (-4:0.1:8);
  options.epsilonk                      = 0.005;
  options.epsilonl                      = 0.001;
  options.epsilonlambda                 = 10e-8;
  options.sigmastart                    = 2;
  options.sigmaend                      = 10e-4;
  options.sigmastretch                  = 10e-3;
  options.threshold                     = 10e-10;
  options.xi                            = 0.1;
  options.nb_iterations                 = 3000;
  options.metric_method                 = 1;
  options.shuffle                       = 1;
  options.updatelambda                  = 1;



  Xtrain                                = [M0(: , ones(1 , Ntrain/2)) + chol(R0)'*randn(d , Ntrain/2) , M1(: , ones(1 , Ntrain/2)) + chol(R1)'*randn(d , Ntrain/2)]; 
  ytrain                                = [zeros(1 , Ntrain/2) , ones(1 , Ntrain/2)];
  [X , Y]                               = meshgrid(vect_test);
  Xtest                                 = [X(:)' ; Y(:)'];

  Nproto_pclass                         = 4*ones(1 , length(unique(ytrain)));


  [Wproto , yproto , lambda]            = ini_proto(Xtrain , ytrain , Nproto_pclass);
  [Wproto_est , yproto_est , lambda_est,  E_SRNG]    = srng_model(Xtrain , ytrain , Wproto , yproto , lambda , options);
  ytest_est                             = NN_predict(Xtest , Wproto_est , yproto_est,lambda_est,options);

  indtrain0                             = (ytrain == 0);
  indtrain1                             = (ytrain == 1);

  indproto0                             = (yproto_est == 0);
  indproto1                             = (yproto_est == 1);

  figure(1)
  imagesc(vect_test , vect_test , reshape(ytest_est , length(vect_test) , length(vect_test)) )
  axis ij
  hold on
  plot(Xtrain(1 , indtrain0) , Xtrain(2 , indtrain0) , 'k+' , Xtrain(1 , indtrain1) , Xtrain(2 , indtrain1) , 'm+' , Wproto_est(1 , indproto0) ,  Wproto_est(2 , indproto0) , 'ko' , Wproto_est(1 , indproto1) ,  Wproto_est(2 , indproto1) , 'mo')
  h = voronoi(Wproto_est(1 , :) , Wproto_est(2 , :));
  set(h , 'color' , 'y' , 'linewidth' , 2)
  hold off
  title('E_{SRNG}(t)' , 'fontsize' , 12)
  colorbar


  figure(2)
  plot(E_SRNG);
  title('E_{SRNG}(t)' , 'fontsize' , 12)


  figure(3)
  stem(lambda_est);
  title('\lambda' , 'fontsize' , 12)



  Example 2
  ---------


  close all
  load ionosphere
  
  options.epsilonk                   = 0.005;
  options.epsilonl                   = 0.001;
  options.epsilonlambda              = 10e-7;
  options.sigmastart                 = 2;
  options.sigmaend                   = 10e-5;
  options.sigmastretch               = 10e-4;
  options.threshold                  = 10e-10;
  options.xi                         = 10;
  options.nb_iterations              = 10000;
  options.metric_method              = 1;
  options.shuffle                    = 1;
  options.updatelambda               = 1;

  [d , N]                            = size(X);
  ON                                 = ones(1 , N);
  mindata                            = min(X , [] , 2);
  maxdata                            = max(X , [] , 2);
  temp                               = maxdata - mindata;
  temp(temp==0)                      = 1;
  X                                  = 2*(X - mindata(: , ON))./(temp(: , ON)) - 1;
  n1                                 = round(0.7*N);
  n2                                 = N - n1;
  ind                                = randperm(length(y));
  ind1                               = ind(1:n1);
  ind2                               = ind(n1+1:N);
  Xtrain                             = X(: , ind1);
  ytrain                             = y(ind1);
  Xtest                              = X(: , ind2);
  ytest                              = y(ind2);
  Nproto_pclass                      = 4*ones(1 , length(unique(y)));
  
  [Wproto , yproto , lambda]         = ini_proto(Xtrain , ytrain , Nproto_pclass);
  [Wproto_est , yproto_est , lambda_est,  E_SRNG]    = srng_model(Xtrain , ytrain , Wproto , yproto , lambda, options);
  ytest_est                          = NN_predict(Xtest , Wproto_est , yproto_est,lambda_est,options);
  Perf                               = sum(ytest == ytest_est)/n2;
  disp(Perf)
  plot(E_SRNG);
  title('E_{SRNG}(t)' , 'fontsize' , 12)
  figure(2)
  stem(lambda_est);
  title('\lambda' , 'fontsize' , 12)





  Example 3
  ---------


  clear,close all hidden
  load artificial

  
  Nproto_pclass                      = [15 , 12 , 3];
  options.epsilonk                   = 0.005;
  options.epsilonl                   = 0.001;
  options.epsilonlambda              = 10e-8;
  options.sigmastart                 = 4;
  options.sigmaend                   = 10e-5;
  options.sigmastretch               = 10e-4;
  options.threshold                  = 10e-10;
  options.xi                         = 1;
  options.nb_iterations              = 25000;
  options.metric_method              = 1;
  options.shuffle                    = 1;
  options.updatelambda               = 1;
  [d , N]                            = size(X);
  ON                                 = ones(1 , N);
  n1                                 = round(0.7*N);
  n2                                 = N - n1;
  ind                                = randperm(length(y));
  ind1                               = ind(1:n1);
  ind2                               = ind(n1+1:N);
  Xtrain                             = X(: , ind1);
  ytrain                             = y(ind1);
  Xtest                              = X(: , ind2);
  
  ytest                              = y(ind2);

  [Wproto , yproto, lambda]          = ini_proto(Xtrain , ytrain , Nproto_pclass);
  [Wproto_est , yproto_est , lambda_est,  E_SRNG]    = srng_model(Xtrain , ytrain , Wproto , yproto , lambda, options);
  ytest_est                          = NN_predict(Xtest , Wproto_est , yproto_est,lambda_est,options);
  Perf                               = sum(ytest == ytest_est)/n2;
  disp(Perf)

  figure(1)
  plot_label(X , y);
  hold on
  h = voronoi(Wproto_est(1 , :) , Wproto_est(2 , :));

  figure(2)
  plot(E_SRNG);
  title('E_{SRNG}(t)' , 'fontsize' , 12)

  figure(3)
  stem(lambda_est);


  
  Example 4
  ---------


  load glass

  options.epsilonk                   = 0.005;
  options.epsilonl                   = 0.001;
  options.epsilonlambda              = 10e-8;
  options.sigmastart                 = 2;
  options.sigmaend                   = 10e-5;
  options.sigmastretch               = 10e-4;
  options.threshold                  = 10e-10;
  options.xi                         = 10;
  options.nb_iterations              = 10000;
  options.metric_method              = 1;
  options.shuffle                    = 1;
  options.updatelambda               = 1;
  [d , N]                            = size(X);
  ON                                 = ones(1 , N);
  mindata                            = min(X , [] , 2);
  maxdata                            = max(X , [] , 2);
  temp                               = maxdata - mindata;
  temp(temp==0)                      = 1;
  X                                  = 2*(X - mindata(: , ON))./(temp(: , ON)) - 1;
  n1                                 = round(0.7*N);
  n2                                 = N - n1;
  ind                                = randperm(length(y));
  ind1                               = ind(1:n1);
  ind2                               = ind(n1+1:N);
  Xtrain                             = X(: , ind1);
  ytrain                             = y(ind1);
  Xtest                              = X(: , ind2);
  ytest                              = y(ind2);
  Nproto_pclass                      = 4*ones(1 , length(unique(y)));

  
  [Wproto , yproto , lambda]         = ini_proto(Xtrain , ytrain , Nproto_pclass);

  [Wproto_est , yproto_est , lambda_est,  E_SRNG]    = srng_model(Xtrain , ytrain , Wproto , yproto , lambda , options);
   
  ytest_est                                          = NN_predict(Xtest , Wproto_est , yproto_est,lambda_est,options);

  Perf                               = sum(ytest == ytest_est)/n2;
  disp(Perf)
  plot(E_SRNG);
  title('E_{SRNG}(t)' , 'fontsize' , 12)

  figure(3)
  stem(lambda_est);






 Author : Sйbastien PARIS : sebastien.paris@lsis.org
 -------  Date : 04/09/2006

 Reference "Margin based Active Learning for LVQ Networks", F.-M. Schleif and B. Hammer and Th. Villmann, ESANN'2006 proceedings


*/


#include <time.h>
#include <math.h>
#include <mex.h>

#define MAX(A,B)   (((A) > (B)) ? (A) : (B) )
#define MIN(A,B)   (((A) < (B)) ? (A) : (B) ) 


#define mix(a , b , c) \
{ \
	a -= b; a -= c; a ^= (c>>13); \
	b -= c; b -= a; b ^= (a<<8); \
	c -= a; c -= b; c ^= (b>>13); \
	a -= b; a -= c; a ^= (c>>12);  \
	b -= c; b -= a; b ^= (a<<16); \
	c -= a; c -= b; c ^= (b>>5); \
	a -= b; a -= c; a ^= (c>>3);  \
	b -= c; b -= a; b ^= (a<<10); \
	c -= a; c -= b; c ^= (b>>15); \
}


#define zigstep 128 // Number of Ziggurat'Steps
#define znew   (z = 36969*(z&65535) + (z>>16) )
#define wnew   (w = 18000*(w&65535) + (w>>16) )
#define MWC    ((znew<<16) + wnew )
#define SHR3   ( jsr ^= (jsr<<17), jsr ^= (jsr>>13), jsr ^= (jsr<<5) )
#define CONG   (jcong = 69069*jcong + 1234567)
#define KISS   ((MWC^CONG) + SHR3)


#ifdef ranKISS
#define randint KISS
#define rand() (randint*2.328306e-10)
#endif 



#ifdef ranSHR3
#define randint SHR3
#define rand() (0.5 + (signed)randint*2.328306e-10)
#endif 


#ifdef __x86_64__
    typedef int UL;
#else
    typedef unsigned long UL;
#endif



static UL jsrseed = 31340134 , jsr;
#ifdef ranKISS
 static UL z=362436069, w=521288629, jcong=380116160;
#endif


static UL iz , kn[zigstep];		
static long hz;
static double wn[zigstep] , fn[zigstep];



typedef struct OPTIONS 
{
  double epsilonk;                      
  double epsilonl;                      
  double epsilonlambda;                 
  double sigmastart;                    
  double sigmaend;                      
  double sigmastretch;
  double threshold;
  double xi;   
  int    nb_iterations;
  int    metric_method;
  int    shuffle;
  int    updatelambda;

} OPTIONS; 



/* Function prototypes */

void randini(void);  
void randnini(void);
double nfix(void);
double randn(void); 
void qs( double * , int , int  ); 
void qsindex( double * , int * , int , int  ); 

void srng_model(double * , double * , OPTIONS , int , int , int , int , 
		        double * , double * , double *, double *, double * , 
			    double * , double * , int *  , double * , int * , int *, double* , double*  , int);





/*-------------------------------------------------------------------------------------------------------------- */



void mexFunction( int nlhs, mxArray *plhs[] , int nrhs, const mxArray *prhs[] )

{
	

    double *Xtrain , *ytrain  , *Wproto , *yproto;

	OPTIONS options = {0.005 , 0.001 , 10e-6 , 2 , 10e-5 , 10e-4 , 10e-10 , 1 , 1000 , 1 , 1 , 1};

	double *Wproto_est , *yproto_est , *E_SRNG , *lambda_est;

	int d , Ntrain  , Nproto  , m = 0;

	int i , j  , l , co , Nprotom  , ld , id , indice;

	double  currentlabel , ind , temp;


	double *tmp , *ytrainsorted , *labels , *mtemp , *stdtemp , *temp_train , *lambda;

	double  *hproto , *sproto , *distj;

	int *Np ;

	int *Nk  , *index_train , *rank_distj , *index_distj;

	int Nproto_max=0;

    mxArray *mxtemp;




   /*Initialize Random generator */
	 
	 randini();	

     randnini(); 



    /* Input 1  Xtrain */
	
	Xtrain    = mxGetPr(prhs[0]);
    		
	if( mxGetNumberOfDimensions(prhs[0]) !=2 )
	{
		
		mexErrMsgTxt("Xtrain must be (d x Ntrain)");
		
	}
	
	d         = mxGetM(prhs[0]);
	 
	Ntrain    = mxGetN(prhs[0]);

	Nproto    = floor(sqrt(Ntrain));

	
	
	/* Input 2  ytrain */
	
	ytrain    = mxGetPr(prhs[1]);
    	
	
	
	if(mxGetNumberOfDimensions(prhs[1]) !=2 || mxGetN(prhs[1]) != Ntrain)
	{
		
		mexErrMsgTxt("ytrain must be (1 x Ntrain)");
		
	}


	/* Determine unique Labels */
	
	ytrainsorted  = mxMalloc(Ntrain*sizeof(double));
	
	
	for ( i = 0 ; i < Ntrain; i++ ) 
	{
		
		ytrainsorted[i] = ytrain[i];
		
	}
	
	
	qs( ytrainsorted , 0 , Ntrain - 1 );
	
	
	labels       = mxMalloc(sizeof(double)); 
	
	labels[m]    = ytrainsorted[0];
	
	currentlabel = labels[0];
	
	for (i = 0 ; i < Ntrain ; i++) 
	{ 
		if (currentlabel != ytrainsorted[i]) 
		{ 
			labels       = (double *)mxRealloc(labels , (m+2)*sizeof(double)); 
			
			labels[++m]  = ytrainsorted[i]; 
			
			currentlabel = ytrainsorted[i];
			
		} 
	} 
	
	m++; 


		


	/* Input 4   yproto */

	if ((nrhs < 4) || mxIsEmpty(prhs[3]) )
		
	{

		plhs[1]               = mxCreateDoubleMatrix(1 , Nproto, mxREAL);
		
		yproto_est            = mxGetPr(plhs[1]);
		
		co                    = 0;
		
		Nprotom               = ceil((double)Nproto/(double)m);
		
		for(i = 0 ; i < m-1 ; i++)
			
		{
			ind             = labels[i];
			
			for(j = 0 ; j < Nprotom ; j++)
				
			{
				
				yproto_est[co]  = labels[i];
				
				co++;
				
			}
			
		}
		
		ind             = labels[m-1];
		
		for(j = (m-1)*Nprotom ; j < Nproto ; j++)
			
		{
			
			yproto_est[co]  = ind;
			
			co++;
			
		}
		
	}
	
	else
	{
			
		
		yproto                = mxGetPr(prhs[3]);
		
		if(mxGetNumberOfDimensions(prhs[3]) !=2)
		{
			
			mexErrMsgTxt("yproto must be (1 x Nproto)");
			
		}

		Nproto                = mxGetN(prhs[3]);

		plhs[1]               = mxCreateDoubleMatrix(1 , Nproto, mxREAL);
		
		yproto_est            = mxGetPr(plhs[1]);


		for( i = 0 ; i < Nproto ; i++)
			
		{
			
			yproto_est[i] = yproto[i];
			
		}

	}
	

	Np           = mxMalloc(m*sizeof(int));
	
	
	for (l = 0 ; l < m ; l++)
		
	{
		
		ind   = labels[l];
		
		Np[l] = 0;
		
		for (i = 0 ; i < Nproto ; i++)
			
		{
			
			if(yproto_est[i] == ind)
				
			{
				
				Np[l]++;
				
			}
		}
		
		if(Np[l] > Nproto_max)
			
		{
			
			Nproto_max = Np[l];
			
		}
		
	}
	




/* Input 3   Wproto */
	
	


	if ((nrhs < 3) || mxIsEmpty(prhs[2]) )
		
	{
				
		mtemp    = mxMalloc(d*m*sizeof(double));
		
		stdtemp  = mxMalloc(d*m*sizeof(double));

    	Nk       = mxMalloc(m*sizeof(int));


		for(i = 0 ; i < d*m ; i++)
		{

			mtemp[i]   = 0.0;

			stdtemp[i] = 0.0;

		}
		


		for (l = 0 ; l < m ; l++)
			
		{
			
			ind   = labels[l];
			
			ld    = l*d;

     		Nk[l] = 0;


			
			for (i = 0 ; i < Ntrain ; i++)
				
			{
				
				if(ytrain[i] == ind)
					
				{
					id  = i*d;
					
					for(j = 0 ; j < d ; j++)
						
					{
						
						mtemp[j + ld] += Xtrain[j + id];
						
					}


					Nk[l]++;

										
				}
			}
				
		}


		for (l = 0 ; l < m ; l++)
			
		{
			
			ld   = l*d;
			
			temp = 1.0/Nk[l];
					
			
			for(j = 0 ; j < d ; j++)
				
			{
				
				mtemp[j + ld] *=temp;
				
				
			}
			
		}
		

		for (l = 0 ; l < m ; l++)
			
		{
			
			ind = labels[l];
			
			ld  = l*d;
			
			for (i = 0 ; i < Ntrain ; i++)
				
			{
				
				if(ytrain[i] == ind)
					
				{
					id  = i*d;
					
					for(j = 0 ; j < d ; j++)
						
					{
						temp             = (Xtrain[j + id] - mtemp[j + ld]);

						stdtemp[j + ld] += (temp*temp);
						
					}
					
					
				}
			}
				
		}



		for (l = 0 ; l < m ; l++)
			
		{
			
			ld   = l*d;
			
			temp = 1.0/(Nk[l] - 1);
					
			
			for(j = 0 ; j < d ; j++)
				
			{
				
				stdtemp[j + ld] = temp*sqrt(stdtemp[j + ld]);
				
				
			}
			
		}

		
		plhs[0]               = mxCreateDoubleMatrix(d , Nproto, mxREAL);
		
		Wproto_est            = mxGetPr(plhs[0]);


		for(l = 0 ; l < Nproto ; l++)
			
		{
			ld = l*d;		
			
			for(i = 0 ; i < m ; i++)
			{
				
				if(labels[i] == yproto_est[l] )
					
				{
					
					indice = i*m;
					
				}
				
				
			}

			
			for(i = 0 ; i < d ; i++)
				
			{
				
				Wproto_est[i + ld]  = mtemp[i + indice] + stdtemp[i + indice]*randn();
				
			}
			
		}
	}
	
	else
	{
		
		
		Wproto                = mxGetPr(prhs[2]);
		
		if(mxGetNumberOfDimensions(prhs[2]) !=2 || mxGetM(prhs[2]) != d)
		{
			
			mexErrMsgTxt("Wproto must be (d x Nproto)");
			
		}

		Nproto                = mxGetN(prhs[2]);

		plhs[0]               = mxCreateDoubleMatrix(d , Nproto, mxREAL);
		
		Wproto_est            = mxGetPr(plhs[0]);
	
		for( i = 0 ; i < d*Nproto ; i++)
			
		{
			
			Wproto_est[i] = Wproto[i];
			
		}
		
	}
	



	
	/* Input 5   lambda */

	plhs[2]                = mxCreateDoubleMatrix(d , 1 , mxREAL);
	
	lambda_est             = mxGetPr(plhs[2]);	


	if ((nrhs < 5) || mxIsEmpty(prhs[4]) )
	{
       
		lambda = (double *)mxMalloc(d*sizeof(double));

		
		for (i = 0 ; i < d ; i++)
		{

			lambda[i]          = 1.0/d;

            lambda_est[i]      = lambda[i] ;

		}

       
	}

	else

	{

    lambda                 = mxGetPr(prhs[4]);
		
	for (i = 0 ; i < d ; i++)
		{

			lambda_est[i]       = lambda[i] ;

		}	
	}


   /* Input 6   option */
	

	if ( (nrhs > 5) && !mxIsEmpty(prhs[5]) )
		
	{
		
				
		mxtemp                                   = mxGetField(prhs[5] , 0, "epsilonk");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.epsilonk                     = tmp[0];
			
		}

		mxtemp                                   = mxGetField(prhs[5] , 0, "epsilonl");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.epsilonl                     = tmp[0];
			
			if (options.epsilonl>options.epsilonk)
			{
				
				
				mexErrMsgTxt("Epsilon_l < Epsilon_k");
				
			}

			
		}
		
		mxtemp                                   = mxGetField(prhs[5] , 0, "epsilonlambda");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.epsilonlambda                = tmp[0];
			
			if (options.epsilonlambda>options.epsilonl/10)
			{
				
				
				mexErrMsgTxt("Epsilon_lambda < Epsilon_l/10");
				
			}

			
		}


		mxtemp                                   = mxGetField(prhs[5] , 0, "sigmastart");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.sigmastart                   = tmp[0];
			
		}

		mxtemp                                   = mxGetField(prhs[5] , 0, "sigmaend");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.sigmaend                     = tmp[0];
			
		}

		mxtemp                                   = mxGetField(prhs[5] , 0, "sigmastretch");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.sigmastretch                 = tmp[0];
			
		}

		
		mxtemp                                   = mxGetField(prhs[5] , 0, "threshold");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.threshold                    = tmp[0];
			
		}
		
		mxtemp                                   = mxGetField(prhs[5] , 0, "xi");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.xi                           = tmp[0];
			
		}
		
		
		
		mxtemp                                   = mxGetField(prhs[5] , 0, "nb_iterations");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.nb_iterations                = (int) tmp[0];
			
		}
		
		mxtemp                                   = mxGetField(prhs[5] , 0, "metric_method");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.metric_method                = (int) tmp[0];
			
		}
	
		
		mxtemp                                   = mxGetField(prhs[5] , 0, "shuffle");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.shuffle                      = (int) tmp[0];
			
		}

		mxtemp                                   = mxGetField(prhs[5] , 0, "updatelambda");
		
		if(mxtemp != NULL)
		{
			
			tmp                                  = mxGetPr(mxtemp);
			
			options.updatelambda                 = (int) tmp[0];
			
		}

		
	}
	
	
	
	
	/*---------- Outputs --------*/ 


			  	
    plhs[3]                = mxCreateDoubleMatrix(1 , options.nb_iterations , mxREAL);
						
	E_SRNG                 = mxGetPr(plhs[3]);


   /*---------- Tempory matrices --------*/ 

	
    temp_train            = mxMalloc(Ntrain*sizeof(double));

    index_train           = mxMalloc(Ntrain*sizeof(int));

	distj                 = mxMalloc(Nproto_max*sizeof(double));

    hproto                = mxMalloc(Nproto_max*sizeof(double));

	sproto                = mxMalloc(Nproto_max*sizeof(double));

    rank_distj            = mxMalloc(Nproto_max*sizeof(int));

    index_distj           = mxMalloc(Nproto_max*sizeof(int));







	/* Main Call */


	srng_model(Xtrain , ytrain , options , d , Ntrain , Nproto , m , 
		       Wproto_est , yproto_est , E_SRNG , lambda , lambda_est , 
			   labels , temp_train , index_train  , distj , rank_distj , index_distj, hproto , sproto , Nproto_max);

		
   /*---------- Free memory --------*/ 

	
	mxFree(labels);
	
	mxFree(ytrainsorted);

	mxFree(temp_train);

	mxFree(index_train);
	
	mxFree(Np);

	mxFree(distj);

	mxFree(rank_distj);

	mxFree(index_distj);

	mxFree(hproto);

	mxFree(sproto);


	if ((nrhs < 3) || mxIsEmpty(prhs[2]) )
		
	{
		
		mxFree(mtemp);
		
		mxFree(stdtemp);

    	mxFree(Nk);
		
	}
	
	if(mxIsEmpty(prhs[4]))
		
	{
		
		mxFree(lambda);
				
	}
	
}


/*-------------------------------------------------------------------------------------------------------------- */

void srng_model(double *Xtrain , double *ytrain , OPTIONS options , int d , int Ntrain , int Nproto , int m , 
				double *Wproto_est , double *yproto_est , double *E_SRNG , double *lambda , double *lambda_est ,
				double *labels , double *temp_train , int *index_train  , double *distj , int *rank_distj, int *index_distj , double *hproto , double* sproto , int Nproto_max)
				
{
	
	
	int i , j , l , t , indice , ind , lmin , offkmin , offlmin , indtemp  , jd;
	
	int coj , rank;
	
	double yi , temp , sum_srng , dlmin , double_max = 1.79769313486231*10e307 ;

	double lnu , cte , ctek , ctel  , ctelamb , tmptmp , tmptmp2, tmptmp1 , cte_tmp;
	
	double sigma , qj  , dist_proto , sum_lambda , dist_temp , weight;
	
	


	hproto[0]        = 1.0; //exp(-0/sigma);
	
	sproto[0]        = 1.0;


	for(i = 0 ; i < Ntrain ; i++)
	{
								
		index_train[i] = i;
		
	}

	
	
	for (t = 0 ; t <options.nb_iterations ; t++)
		
		
	{
		
		// Shuffle Train data //
		if(options.shuffle)
			
		{
			
			for(i = 0 ; i < Ntrain ; i++)
			{
				
				temp_train[i]  = rand();
				
				index_train[i] = i;
				
			}
			
			qsindex(temp_train , index_train , 0 , Ntrain - 1 ); 
			
		}
		
		
		// Update neighbourg size ///
		
		
		sigma            = (options.sigmaend - options.sigmastart) *(1.0 - 2.0/(1.0 + exp(t*options.sigmastretch))) + options.sigmastart;

			
		
		for (i = 1 ; i < Nproto_max ; i++)
		{
			
			hproto[i]    = exp(-i/sigma);
			
			sproto[i]    = hproto[i] + sproto[i - 1];     
			
		}
		
		
		
		// Compute distance Wproto, Xtrain
		
		E_SRNG[t]        = 0.0;	
		
		for(i = 0 ; i < Ntrain ; i++)
			
		{
			
			indice       = index_train[i];
			
			ind          = indice*d;
			
			yi           = ytrain[indice];
			
			dlmin        = double_max;
			
			lmin         = 0;
			
			coj          = 0;
			
			
			
			for(j = 0 ; j < Nproto ; j++)
				
			{
				
				dist_proto = 0.0;
				
				jd         = j*d;
				
				if (options.metric_method)
				{
					
					for(l = 0 ; l < d ; l++)
					{
						
						temp        = (Xtrain[l + ind] - Wproto_est[l + jd]);

						dist_proto += lambda_est[l]*temp*temp;
				
						
					}
					
				}
				
				else
				{
					for(l = 0 ; l < d ; l++)
					{
						
						temp        = (Xtrain[l + ind] - Wproto_est[l + jd]);

						dist_proto += lambda_est[l]*temp*temp*temp*temp;					
						
						
					}
				}
				
				if(yproto_est[j] == yi)
				{
					
					distj[coj]       = dist_proto;
					
					index_distj[coj] = j;
					
					coj++;
					
				}
				
				else
					
				{
					
					if(dist_proto < dlmin)
						
					{
						
						dlmin        = dist_proto;
						
						lmin         = j;
						
					}
					
				}
				
			}
			
			
			
			for(j = 0 ; j < coj ; j++)
			{
				
				rank_distj[j]   = j;
				
			}
			
			qsindex(distj , rank_distj , 0 , coj - 1 ); 
			
			sum_lambda     = 0.0;
			
			sum_srng       = 0.0;
			
			temp           = 1.0/sproto[coj - 1];
			
			offlmin        = lmin*d;
			
			
			for(j = 0 ; j < coj ; j++)
				
			{
				
				
				rank       = rank_distj[j];
				
				weight     = hproto[j]*temp;
					
				
				if (weight > options.threshold)
				{
					
					dist_temp  = distj[j];
							
					tmptmp     = 1.0/(dist_temp + dlmin);
					
					qj         = (dist_temp - dlmin)*tmptmp;
					
					lnu        = 1.0/(1.0 + exp(-options.xi*qj));
					
					cte        = lnu*weight; 
					
					sum_srng  += cte;


									
					// cte_tmp    = options.xi*cte*(1.0 - lnu)*tmptmp*tmptmp;
					
					cte_tmp    = options.xi*cte*(1.0 - lnu)*tmptmp;				
					
					ctek       = options.epsilonk*cte_tmp*dlmin;
					
					ctel       = options.epsilonl*cte_tmp;
					
					ctelamb    = options.epsilonlambda*cte_tmp;
					
					offkmin    = index_distj[rank]*d;
										
					
					for( l = 0 ; l < d ; l++)
						
					{
						
						indtemp               = l + offkmin;
						
						tmptmp1               = Xtrain[l + ind] - Wproto_est[indtemp];
						
						Wproto_est[indtemp]  += 4.0*ctek*lambda_est[l]*tmptmp1; 
						
						
						indtemp               = l + offlmin;
						
						tmptmp2               = Xtrain[l + ind] - Wproto_est[indtemp];
						
						Wproto_est[indtemp]  -= 4.0*ctel*dist_temp*lambda_est[l]*tmptmp2;
						
						if (options.updatelambda)
						{
														
							lambda_est[l]        -= ctelamb*2.0*(dlmin*tmptmp1*tmptmp1 - dist_temp*tmptmp2*tmptmp2); 
							
						}
						
					}
					
				}
				
			}
			
			
			
			E_SRNG[t]    += sum_srng;
			
			if (options.updatelambda)
			{
				
				
				for (l = 0 ; l < d ; l++)
				{
					
					lambda_est[l]         = MAX(0 , lambda_est[l]);
					
					sum_lambda           += lambda_est[l];			
					
				}
				
				
				sum_lambda         = 1.0/sum_lambda;
				
				for (l = 0 ; l < d ; l++)
				{
					
					lambda_est[l] *= sum_lambda;
					
				}
			}
       }
	     
	   
	}
	
}



/*----------------------------------------------------------------------------------------------------------------------------------------- */


void qs(double  *a , int lo, int hi)
{
//  lo is the lower index, hi is the upper index
//  of the region of array a that is to be sorted
    int i=lo, j=hi;
    double x=a[(lo+hi)/2] , h;

    //  partition
    do
    {    
        while (a[i]<x) i++; 
        while (a[j]>x) j--;
        if (i<=j)
        {
            h        = a[i]; 
			a[i]     = a[j]; 
			a[j]     = h;
            i++; 
			j--;
        }
    }
	while (i<=j);

    //  recursion
    if (lo<j) qs(a , lo , j);
    if (i<hi) qs(a , i , hi);
}

/*----------------------------------------------------------------------------------------------------------------------------------------- */

void qsindex (double  *a, int *index , int lo, int hi)
{
//  lo is the lower index, hi is the upper index
//  of the region of array a that is to be sorted
    int i=lo, j=hi , ind;
    double x=a[(lo+hi)/2] , h;

    //  partition
    do
    {    
        while (a[i]<x) i++; 
        while (a[j]>x) j--;
        if (i<=j)
        {
            h        = a[i]; 
			a[i]     = a[j]; 
			a[j]     = h;
			ind      = index[i];
			index[i] = index[j];
			index[j] = ind;
            i++; 
			j--;
        }
    }
	while (i<=j);

    //  recursion
    if (lo<j) qsindex(a , index , lo , j);
    if (i<hi) qsindex(a , index , i , hi);
}

/* --------------------------------------------------------------------------- */

 
 void randini(void) 
	 
 {
	 
	 /* SHR3 Seed initialization */
	 
	 jsrseed  = (UL) time( NULL );
	 
	 jsr     ^= jsrseed;
	 
	 
	 /* KISS Seed initialization */
	 
#ifdef ranKISS
	  
	 z        = (UL) time( NULL );
	 
	 w        = (UL) time( NULL ); 
	 
	 jcong    = (UL) time( NULL );
	 
	 mix(z , w , jcong);
	 
#endif 
	 
	 
 }
 

/* --------------------------------------------------------------------------- */

void randnini(void) 
{	  
	register const double m1 = 2147483648.0;

	register double  invm1;

	register double dn = 3.442619855899 , tn = dn , vn = 9.91256303526217e-3 , q; 
	
	int i;

	
	/* Ziggurat tables for randn */	 
	
	invm1             = 1.0/m1;
	
	q                 = vn/exp(-0.5*dn*dn);  
	
	kn[0]             = (dn/q)*m1;	  
	
	kn[1]             = 0;
		  
	wn[0]             = q*invm1;	  
	
	wn[zigstep - 1 ]  = dn*invm1;
	
	fn[0]             = 1.0;	  
	
	fn[zigstep - 1]   = exp(-0.5*dn*dn);		
	
	for(i = (zigstep - 2) ; i >= 1 ; i--)      
	{   
		dn              = sqrt(-2.*log(vn/dn + exp(-0.5*dn*dn)));          
	
		kn[i+1]         = (dn/tn)*m1;		  
		
		tn              = dn;          
		
		fn[i]           = exp(-0.5*dn*dn);          
		
		wn[i]           = dn*invm1;      
	}

}


/* --------------------------------------------------------------------------- */


double nfix(void) 
{	
	const double r = 3.442620f; 	/* The starting of the right tail */	
	
	static double x, y;
	
	for(;;)

	{
		
		x = hz*wn[iz];
					
		if(iz == 0)
		
		{	/* iz==0, handle the base strip */
			
			do
			{	
				x = -log(rand())*0.2904764;  /* .2904764 is 1/r */  
								
				y = -log(rand());			
			} 
			
			while( (y + y) < (x*x));
			
			return (hz > 0) ? (r + x) : (-r - x);	
		}
		
		if( (fn[iz] + rand()*(fn[iz-1] - fn[iz])) < ( exp(-0.5*x*x) ) ) 
		
		{
		
			return x;

		}

		
		hz = randint;		

		iz = (hz & (zigstep - 1));		
		
		if(abs(hz) < kn[iz]) 

		{
			return (hz*wn[iz]);	

		}


	}

}


/* --------------------------------------------------------------------------- */


double randn(void) 
	
{ 

		hz = randint;
		
		iz = (hz & (zigstep - 1));

		return (abs(hz) < kn[iz]) ? (hz*wn[iz]) : ( nfix() );
	
}



