function cyto=SegmentCyto4(X);
%%this function segments the cytoplasm of leukocytes
%it is based on topology edge finding and thresholding operations
%should be faster and less error-prone than the previous one

a=5;
[w,h,z]=size(X);
%%canals
Y=rgb2hsv(X);
H=Y(:,:,1);
S=Y(:,:,2);
V=Y(:,:,3);

%%retrieve nucleus
kern=fspecial('gaussian',11,5);
%blurS=imfilter(S,kern);
nuke=im2bw(S,mean(mean(S)));
%[xg,yg]=gravity_center(nuke,w,h);
%nuke=bwselect(nuke,yg,xg);
dnuke=nuke;%bwmorph(nuke,'dilate',a);

%%contrast
H=H-min(min(H));
H=H./max(max(H));
V=V-min(min(V));
V=V./max(max(V));

%%get the mass of the cytoplasm
bw=im2bw(H,0.5);

%%get the boundaries
blurV=imfilter(V,kern);
edV=edge(blurV,'canny');
edV(1:a,:)=0;%filter compensation
edV(:,1:a)=0;%filter compensation
edV(:,w-a:end)=0;
edV(w-a:end,:)=0;

%%topological treatment
dil=bwmorph(edV,'dilate',a);
%shr=bwmorph(dil,'thin',9001);%its over 9000!!!
%ero=bwmorph(dil,'erode',a);

%%combination
exclu=bw-dil;
exclu=im2bw(exclu,0.5);%discard negatives
exclu=or(exclu,dnuke);

exclu=bwfill(exclu,'holes');

%%select
[xg,yg]=gravity_center(exclu,w,h);
sel=bwselect(exclu,yg,xg,4);%4-connectivity
%di=bwmorph(sel,'dilate',a);
%er=bwmorph(di,'erode',a);

fill=bwmorph(sel,'dilate',a);
fill=bwfill(fill,'holes');

%%return
cyto=fill-nuke;
cyto=im2bw(cyto,0.5);%discard negative