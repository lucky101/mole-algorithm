clearall
 
x=imread('I2519.bmp');
%x=rgb2gray(x); 
x=im2single(x);
 
[m,n]=size(x);
 
h=fspecial('gaussian',15,1.9);
 
laph=fspecial('log',17,2);
 
laph=laph-mean2(laph);
 
y1=imfilter(x,laph,'replicate');
 
%yy=(y1- mean2(y1));
 
yy=y1.^0.7;
 
imshow(yy+0.5);
 
e = false(m,n);
 
% finding zero crossing
 
% Look for the zero crossings: +-, -+ and their transposes% We arbitrarily choose the edge to be the negative point
 
thresh=0.065*max(max(y1));
 
rr = 2:m-1; cc=2:n-1;
 
[rx,cx] = find( y1(rr,cc) < 0 & y1(rr,cc+1) > 0...
 
& abs( y1(rr,cc)-y1(rr,cc+1) ) > thresh );% [- +]
 
e((rx+1) + cx*m) = 1;
 
[rx,cx] = find( y1(rr,cc-1) > 0 & y1(rr,cc) < 0...
 
& abs( y1(rr,cc-1)-y1(rr,cc) ) > thresh );% [+ -]
 
e((rx+1) + cx*m) = 1;
 
[rx,cx] = find( y1(rr,cc) < 0 & y1(rr+1,cc) > 0...
 
& abs( y1(rr,cc)-y1(rr+1,cc) ) > thresh);% [- +]'
 
e((rx+1) + cx*m) = 1;
 
[rx,cx] = find( y1(rr-1,cc) > 0 & y1(rr,cc) < 0...
 
& abs( y1(rr-1,cc)-y1(rr,cc) ) > thresh);% [+ -]'
 
e((rx+1) + cx*m) = 1;
 
% Most likely this covers all of the cases. Just check to see ifthere
 
% are any points where the LoG was precisely zero:
 
[rz,cz] = find( y1(rr,cc)==0 );
 
if~isempty(rz)
 
% Look for the zero crossings: +0-, -0+ and their transposes
 
% The edge lies on the Zero point
 
zero = (rz+1) + cz*m;% Linear index for zero points
 
zz = find(y1(zero-1) < 0 & y1(zero+1) > 0...
 
& abs( y1(zero-1)-y1(zero+1) ) > 2*thresh);% [- 0+]'
 
e(zero(zz)) = 1;
 
zz = find(y1(zero-1) > 0 & y1(zero+1) < 0...
 
& abs( y1(zero-1)-y1(zero+1) ) > 2*thresh);% [+ 0 -]'
 
e(zero(zz)) = 1;
 
zz = find(y1(zero-m) < 0 & y1(zero+m) > 0...
 
& abs( y1(zero-m)-y1(zero+m) ) > 2*thresh);% [- 0+]
 
e(zero(zz)) = 1;
 
zz = find(y1(zero-m) > 0 & y1(zero+m) < 0...
 
& abs( y1(zero-m)-y1(zero+m) ) > 2*thresh);% [+ 0 -]
 
e(zero(zz)) = 1;
 
end
 
figure(2);
 
imshow(e)
 