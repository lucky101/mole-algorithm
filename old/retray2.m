%clear all; close all; clc;
img = imread('I2519.bmp');
t=45; % Thres Hold value
s = 55; % Seed Value
f=rgb2gray(img);
[g, nr,si,ti] = regiongrow(f,s,t);
%figure,imshow(f),title('Input Image');
%figure,imshow(ti),title('After Segmentation');

se90=strel('line', 3, 90);
se0=strel('line', 3, 0);      
BWsdil=imdilate(ti, [se90 se0]);
%figure, imshow(BWsdil), title('dilated gradient mask');
BWdfill=imfill(BWsdil, 'holes');
%figure, imshow(BWdfill); title('binary image with filled holes');
BWnobord=imclearborder(BWdfill, 8);
%figure, imshow(BWnobord), title('cleared border image');
seD=strel('diamond', 7);
BWfinal=imerode(BWnobord, seD);
BWfinal=imerode(BWfinal, seD);
%figure, imshow(BWfinal)

BWoutline=bwperim(BWfinal);
Segout=img;
Segout(BWoutline)=0; 
figure, imshow(Segout), title('Границы изображения');


 
