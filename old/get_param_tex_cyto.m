function tex_cyto=get_param_tex_cyto(X,Y)
%%get_param_tex_cyto.m
%CYTOPLASM
%this functions uses the image of the cell in RGB and SATURATION
%to retrieve into a line vector the texture parameters used for
%leukocyte classification
%   cyto_corr_70, cyto_hom_70, cyto_con_70, cyto_ene_70];

Y=Y(:,:,2);
cyto=SegmentCyto7(X,1);%is worth checkin if rgb2hsv is not redone here
vX=im2double(cyto).*Y;
vX=vX-min(min(vX));
vX=vX./max(max(vX));

%%get the textures
a=70; %texture element size
gray00=graycomatrix(vX, 'Offset', [0 a]);
gray45=graycomatrix(vX, 'Offset', [-a a]);
gray90=graycomatrix(vX, 'Offset', [-a 0]);
gray135=graycomatrix(vX, 'Offset', [-a -a]);

props00=graycoprops(gray00);
props45=graycoprops(gray45);
props90=graycoprops(gray90);
props135=graycoprops(gray135);
%make them isotropic!
Correlation=mean([props00.Correlation, props45.Correlation, props90.Correlation, props135.Correlation]);
Energy=mean([props00.Energy, props45.Energy, props90.Energy, props135.Energy]);
Homogeneity=mean([props00.Homogeneity, props45.Homogeneity, props90.Homogeneity, props135.Homogeneity]);
Contrast=mean([props00.Contrast, props45.Contrast, props90.Contrast, props135.Contrast]);

%   cyto_corr_70, cyto_hom_70, cyto_con_70, cyto_ene_70];
tex_cyto=[Correlation, Homogeneity, Contrast, Energy];