function [xg,yg]=gravity_center(bw,w,h);
%%this function computes the gravity center of a binary image
%recieves the image, the width and the height
nbpts=0;
xg=0;
yg=0;
for i=1:h-1
    for j=1:w-1
        pixVal=bw(i,j);
        if(pixVal>0)
            xg=xg+j;
            yg=yg+i;
            nbpts=nbpts+1;
        end
    end
end
xg=round(xg/nbpts);
yg=round(yg/nbpts);