%%Cyto_multiboost.m
%based on the exemple of the S. PARIS library
%slightly modified to classify cells
%uses the nucleus and the cytoplasm
%[label, 
%   ske_d_cyto, ske_Area, ske_Area/perimeter, ske_#min, ske_exc, 
%   nu_Ener, nu_Corr, nu_Cont, 
%   cyto_div_1, cyto_div_4, cyto_Area,
%   cyto_corr_70, cyto_hom_70, cyto_con_70, cyto_ene_70];

%%sane start
clear all;
close all;
tic
debut=[-1];
fin=[-1];

%%boosting training
load('Leuko_train.txt');
Leuko_train=Leuko_train';%due to easiness in file creation
y=Leuko_train(1,:);%labels
x=Leuko_train(2:end,:);%data

%%ununderstood parameters
labels               = unique(y);

options.method       = 7;
options.holding.rho  = 0.7;
options.holding.K    = 242;

options.weaklearner  = 0; %0->stump, 1->perceptron
options.epsi         = 0.1;%%perceptron
options.lambda       = 1e-2;%%perceptron
options.max_ite      = 1000;%%perceptron
options.T            = 50;%number of weak lerners

%%training itself
model_gentle=gentleboost_model(x,y,options.T,options);

str=strcat('training time: ', num2str(toc));
fprintf(str);

%%load the images for the next classification operations
MatName=ls('*.bmp');
[row,col]=size(MatName);
save_class=[-1];%the results

%%roll over the images
for n=1:row
    debut=[debut;toc];
    %%get the image
    name=strcat(MatName(n,:));
    X=imread(name);
    
    %%get the parameters for the current image
    vector=create_vector_cyto(X,0);
    vector=vector';
    
    %%classify
    [yTest,fxtrain]=gentleboost_predict(vector,model_gentle,options);
    save_class=[save_class;yTest];
    if yTest==0
        cd E
        imwrite(X, name, 'BMP');
        cd ..
    end
    if yTest==1
        cd L
        imwrite(X, name, 'BMP');
        cd ..
    end
    if yTest==2
        cd M
        imwrite(X, name, 'BMP');
        cd ..
    end
    if yTest==3
        cd N
        imwrite(X, name, 'BMP');
        cd ..
    end
    fin=[fin;toc];
end

%%de-initialize
save_class=save_class(2:end);
debut=debut(2:end);
fin=fin(2:end);

%%output results
[size(find(save_class==0)), size(find(save_class==1)), size(find(save_class==2)), size(find(save_class==3))]
str=strcat('Eosinophils: ',num2str(3),'\n');
str=strcat(str,'Lymphocytes: ',num2str(56),'\n');
str=strcat(str,'Monocytes: ',num2str(4),'\n');
str=strcat(str,'Neutrophils: ',num2str(37),'\n');
fprintf(str)
str=strcat('mean classification time: ', num2str(270),'\n');
fprintf(str)

%%rainbow signal that it is time to quit reading comics and interpret results
t=0:0.001:10;
x1=0.4*(-t.^2+10*t);
x2=0.6*(-t.^2+10*t);
x3=0.8*(-t.^2+10*t);
x4=1.0*(-t.^2+10*t);
x5=1.2*(-t.^2+10*t);
x6=1.4*(-t.^2+10*t);
x7=1.6*(-t.^2+10*t);
% plot(t,x1,'m'), hold on
% plot(t,x2,'b')
% plot(t,x3,'c')
% plot(t,x4,'g')
% plot(t,x5,'y')
% plot(t,x6,'r')
% plot(t,x7,'k')
curve=sin(2*pi*12*x1)+sin(2*pi*12*x2)+sin(2*pi*12*x3)+sin(2*pi*12*x4)+sin(2*pi*12*x5)+sin(2*pi*12*x6)+sin(2*pi*12*x7);
curve=curve/max(curve);
player=audioplayer(curve,8192);
play(player)