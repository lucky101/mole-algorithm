function tex_nuke=get_param_tex_nuke(Y)
%%get_param_tex_nuke.m
%NUCLEUS
%this functions uses the image of the cell in SATURATION channel
%to retrieve into a line vector the texture parameters used for
%leukocyte classification
%   nu_Ener, nu_Corr, nu_Cont,

%%get the textures
a=140; %texture element size
gray0=graycomatrix(Y, 'Offset', [0 a]);
gray45=graycomatrix(Y, 'Offset', [-a a]);
gray90=graycomatrix(Y, 'Offset', [-a 0]);
gray135=graycomatrix(Y, 'Offset', [-a -a]);

props0=graycoprops(gray0);
props45=graycoprops(gray45);
props90=graycoprops(gray90);
props135=graycoprops(gray135);
%make them isotropic!
Correlation=mean([props0.Correlation, props45.Correlation, props90.Correlation, props135.Correlation]);
Correlation=abs(Correlation);
Energy=mean([props0.Energy, props45.Energy, props90.Energy, props135.Energy]);
%Homogeneity=mean([props0.Homogeneity, props45.Homogeneity, props90.Homogeneity, props135.Homogeneity]);
Contrast=mean([props0.Contrast, props45.Contrast, props90.Contrast, props135.Contrast]);

%   nu_Ener, nu_Corr, nu_Cont,
tex_nuke=[Energy, Correlation, Contrast];