%%create_TSet_cyto.m
%creates the training set on the basis of the present images
%using the create_vector_cyto function to create the characteristic vector
%in order to create a training set that could be used for
%multi-class adaboost
%everything will be saved in a text file under the format (line vector):
%[label, 
%   ske_d_cyto, ske_Area, ske_Area/perimeter, ske_#min, ske_exc, 
%   nu_Ener, nu_Corr, nu_Cont, 
%   cyto_div_1, cyto_div_4, cyto_Area,
%   cyto_corr_70, cyto_hom_70, cyto_con_70, cyto_ene_70];
%   cyto_el, cyto_H, cyto_S];
%1 label, 5 skeleton parameters, 3 nucleus texutre parameters, 
%2 quad tree cytoplasm parameters and 3 cytoplasm texture parameters
%the file is saved in lines, hences requires transposition before training
%is it interesting to use the nucleus' surface?


clear all
close all

train=4;%define the label

%%load images
MatName=ls('*.bmp');
[row,col]=size(MatName); %the number of files

%%open saving file
id=fopen('Leuko_train.txt','a');

for n=1:row
%%run over all the images

    %get image
    name=strcat(MatName(n,:));
    X=imread(name);
    

    %create vector
    vector=create_vector_cyto(X,train);
    
    %write the 18 parameters to file
    fprintf(id,'%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f\r\n',vector);
    
end

%%close saving file
fclose(id);

%%rainbow signal that it is time to quit reading comics and interpret results
t=0:0.001:10;
x1=0.4*(-t.^2+10*t);
x2=0.6*(-t.^2+10*t);
x3=0.8*(-t.^2+10*t);
x4=1.0*(-t.^2+10*t);
x5=1.2*(-t.^2+10*t);
x6=1.4*(-t.^2+10*t);
x7=1.6*(-t.^2+10*t);
% plot(t,x1,'m'), hold on
% plot(t,x2,'b')
% plot(t,x3,'c')
% plot(t,x4,'g')
% plot(t,x5,'y')
% plot(t,x6,'r')
% plot(t,x7,'k')
curve=sin(2*pi*12*x1)+sin(2*pi*12*x2)+sin(2*pi*12*x3)+sin(2*pi*12*x4)+sin(2*pi*12*x5)+sin(2*pi*12*x6)+sin(2*pi*12*x7);
curve=curve/max(curve);
player=audioplayer(curve,8192);
play(player)