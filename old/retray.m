close all
p=imread('I2519.bmp');
B = rgb2gray(p);
kl=180;                                                     % порог для определения центров кристилизации на изображении
S=double(B);                                                
[m,n]=size(S);  
figure,imshow(p) 
%figure,imshow(B) 
% Создаю массив с центрами кристалицации, т.е. если элемент > kl то присваиваю ему 1
for i=1:m 
    for j=1:n 
       if S(i,j)>=180 S(i,j)=1;
else
    S(i,j)=0;
       end
    end
end
%imwrite(S, 'C:\Users\Desktop\1\00k.jpg');    
figure,imshow(S) 
% выращивание областей
f=double(B);
if numel(S)==1
        SI=f==S;
   S1=S;
else
   SI=bwmorph(S, 'shrink', Inf);
    J=find(SI);
   S1=f(J);
end
TI=false(size(f));
 
for K=1:length(S1)                            % ВОТ ЭТА ЧАСТЬ ПРОГРАММЫ РАБОТАЕТ ОЧЕНЬ МЕДЛЕННО
   seedvalue=S1(K);
   T=80;
   S=abs(f-seedvalue)<=T;
   TI=TI | S;
end
 
[g, NR]=bwlabel(imreconstruct(SI, TI));
figure,imshow(g) 

%imwrite(g, 'C:\Users\Desktop\1\bw.jpg');      