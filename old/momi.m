close all;
clear; 
%%Открытие исходного изображения
%A = imread('2519.JPG','jpg');
%imwrite(A,'O2519.bmp','bmp'); 
%img=imread('O2519.bmp');
img=imread('CS2.jpg');
				
vonkoch=img(1:510);
len=length(vonkoch);
[c,l]=wavedec(vonkoch,5,'haar');
% Compute and reshape DWT to compare with CWT.
cfd=zeros(5,len);
for k=1:5
    d=detcoef(c,l,k);
    d=d(ones(1,2^k),:);
    cfd(k,:)=wkeep(d(:)',len);
end
cfd=cfd(:);
I=find(abs(cfd) <sqrt(eps));
cfd(I)=zeros(size(I));
cfd=reshape(cfd,5,len);
% Plot DWT.
subplot(311); plot(vonkoch); title('Analyzed signal.');
set(gca,'xlim',[0 510]);
subplot(312); 
image(flipud(wcodemat(cfd,255,'row')));
colormap(pink(255));
set(gca,'yticklabel',[]);
title('Discrete Transform,absolute coefficients');
ylabel('Level');
% Compute CWT and compare with DWT
subplot(313);
ccfs=cwt(vonkoch,1:32,'haar','plot');
title('Continuous Transform, absolute coefficients');
set(gca,'yticklabel',[]);
ylabel('Scale');

[c,s] = wavefast(img,1,'sym4');
figure; wave2gray(c,s,-6);
[nc,y] = wavecut('a',c,s);
figure; wave2gray(nc,s,-6);
edges = abs(waveback(nc,s,'sym4'));
figure; imshow(mat2gray(edges));
