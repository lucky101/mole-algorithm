%%Classificator_boost_multi.m
%based on the exemple of the S. PARIS library
%slightly modified to classify cells

clear all;
close all;


%%retrieve data
load('Leuko_train.txt');
debut=[-1];
fin=[-1];
save_class=[-1];
tic

Leuko_train=Leuko_train';%due to easiness in file creation
y=Leuko_train(1,:);
x=Leuko_train(2:end,:);

%%ununderstood parameters
labels               = unique(y);

options.method       = 7;
options.holding.rho  = 0.7;
options.holding.K    = 242;

options.weaklearner  = 0; %0->stump, 1->perceptron
options.epsi         = 0.1;%%perceptron
options.lambda       = 1e-2;%%perceptron
options.max_ite      = 1000;%%perceptron
options.T            = 100;%number of weak lerners

% positive             = labels(2);
% ind_positive         = find(labels==positive);
% 
% [d , N]              = size(X);
% [Itrain , Itest]     = sampling(X , y , options);
% 
% [Ncv , Ntrain]       = size(Itrain);
% Ntest                = size(Itest , 2);
% error_train          = zeros(1 ,  Ncv);
% 
% error_test           = zeros(1 ,  Ncv);
% 
% tptrain              = zeros(Ncv , 100);
% fptrain              = zeros(Ncv , 100);
% 
% tptest               = zeros(Ncv , 100);
% fptest               = zeros(Ncv , 100);

%%load images
MatName=ls('*.bmp');
[row,col]=size(MatName); %the number of files

%%open saving file
id=fopen('Leuko_train.txt','a');

model_gentle=gentleboost_model(x,y,options.T,options);

for n=1:row
%%run over all the images

    %get image
    name=strcat(MatName(n,:));
    X=imread(name);
    Y=rgb2hsv(X);
    
    %tempus fugit
    debut=[debut;toc];
    
    vector=create_vector(Y(:,:,2),0);
    vector=vector';
    
    [yTest,fxtrain]=gentleboost_predict(vector,model_gentle,options);
    save_class=[save_class;yTest];
    
    %tempus fugit
    fin=[fin;toc];
end

temps=mean(fin-debut);
save_class=save_class(2:end);
temps
[size(find(save_class==0)), size(find(save_class==1)), size(find(save_class==2)), size(find(save_class==3))]
str=strcat('Eosinophils: ',num2str(size(find(save_class==0),1)),'\n');
str=strcat(str,'Lymphocytes: ',num2str(size(find(save_class==1),1)),'\n');
str=strcat(str,'Monocytes: ',num2str(size(find(save_class==2),1)),'\n');
str=strcat(str,'Neutrophils: ',num2str(size(find(save_class==3),1)),'\n');
fprintf(str)

%%rainbow signal that it is time to quit reading comics and interpret results
t=0:0.001:10;
x1=0.4*(-t.^2+10*t);
x2=0.6*(-t.^2+10*t);
x3=0.8*(-t.^2+10*t);
x4=1.0*(-t.^2+10*t);
x5=1.2*(-t.^2+10*t);
x6=1.4*(-t.^2+10*t);
x7=1.6*(-t.^2+10*t);
% plot(t,x1,'m'), hold on
% plot(t,x2,'b')
% plot(t,x3,'c')
% plot(t,x4,'g')
% plot(t,x5,'y')
% plot(t,x6,'r')
% plot(t,x7,'k')
curve=sin(2*pi*12*x1)+sin(2*pi*12*x2)+sin(2*pi*12*x3)+sin(2*pi*12*x4)+sin(2*pi*12*x5)+sin(2*pi*12*x6)+sin(2*pi*12*x7);
curve=curve/max(curve);
player=audioplayer(curve,8192);
play(player)