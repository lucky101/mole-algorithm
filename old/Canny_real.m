close all;
clear; 
%%Открытие исходного изображения
%A = imread('2519.JPG','jpg');
%imwrite(A,'O2519.bmp','bmp'); 
%img=imread('O2519.bmp');
img=imread('2519.bmp');
%%Нахождение R,G,B компонентов
R = im2double(img (:,:,1));
G = im2double(img (:,:,2));
B = im2double(img(:,:,3));

grayimg = rgb2gray(img);%%Преобразование в оттенки серого
%figure, imshow(grayimg)
grayimg = imadjust(grayimg);%%Улучшение качества изображения
%figure, imshow(grayimg)
%%----------------------------------------------------------------------
%%Нахождение эталона
%%----------------------------------------------------------------------
bw = edge(grayimg,'canny', 0.15, 10);
bw = imfill(bw,'holes');
se = strel('disk',1); 
bw = imopen(bw,se);
I=im2bw(grayimg,0.7);
%figure, imshow(I)
P=1-I;
%figure, imshow(P)
[A,L] = bwboundaries(bw);
stats = regionprops(L,'Centroid','EquivDiameter');
figure, imshow(img)
hold on

vector=zeros(5,1);
coordinat=zeros(5,2);

for k = 1:length(A)
 boundary = A{k};
 radius = stats(k).EquivDiameter/2;
 if radius > 29 
 vector(k,1)=radius;
 
 xc = stats(k).Centroid(1);
 yc = stats(k).Centroid(2);
 
 coordinat(k,1)=xc;
 coordinat(k,2)=yc;
 
 theta = 0:0.01:2*pi;
 Xfit = radius*cos(theta) + xc;
 Yfit = radius*sin(theta) + yc;
 %plot(Xfit, Yfit, 'g');
 %text(boundary(1,2)-15,boundary(1,1)+15, num2str(radius,3),'Color','y',...
 % 'FontSize',8);
 end
end

%%Нахождение вектора радиусов(в пикселях) эталона
vector(vector==0)=[];
%display(vector);

%%Нахождение координат центров эталона
coordinat(coordinat==0)=[];
coordinat=reshape(coordinat,5,2);
%display(coordinat);

%--------------------------------------------------------------------
%Находим все 5 кругов эталона и их цветовые характеристики
%--------------------------------------------------------------------

%1-й круг
circle1x=coordinat(1,1);
circle1y=coordinat(1,2);
circle1=bwselect(P,circle1x,circle1y);
%figure, imshow(circle1) 
Color1r=mean(R(circle1(:)));
Color1g=mean(G(circle1(:)));
Color1b=mean(B(circle1(:)));
Color1=[1,Color1r,Color1g,Color1b];

%2-й круг
circle2x=coordinat(2,1);
circle2y=coordinat(2,2);
circle2=bwselect(P,circle2x,circle2y);
%figure, imshow(circle2) 
Color2r=mean(R(circle2(:)));
Color2g=mean(G(circle2(:)));
Color2b=mean(B(circle2(:)));
Color2=[2,Color2r,Color2g,Color2b];

%3-й круг
circle3x=coordinat(3,1);
circle3y=coordinat(3,2);
circle3=bwselect(P,circle3x,circle3y);
%figure, imshow(circle3) 
Color3r=mean(R(circle3(:)));
Color3g=mean(G(circle3(:)));
Color3b=mean(B(circle3(:)));
Color3=[3,Color3r,Color3g,Color3b];

%4-й круг
circle4x=coordinat(4,1);
circle4y=coordinat(4,2);
circle4=bwselect(P,circle4x,circle4y);
%figure, imshow(circle4) 
Color4r=mean(R(circle4(:)));
Color4g=mean(G(circle4(:)));
Color4b=mean(B(circle4(:)));
Color4=[4,Color4r,Color4g,Color4b];

%5-й круг
circle5x=coordinat(5,1);
circle5y=coordinat(5,2);
circle5=bwselect(P,circle5x,circle5y);
%figure, imshow(circle5) 
Color5r=mean(R(circle5(:)));
Color5g=mean(G(circle5(:)));
Color5b=mean(B(circle5(:)));
Color5=[5,Color5r,Color5g,Color5b];


%______________________________________________________________________
%Определение красного цвета в эталоне
%______________________________________________________________________
Red=[Color1r,Color2r,Color3r,Color4r,Color5r];

Rmax=0;
Jmax=0;
for i=1:5
    if Red(i)>Rmax
        Rmax=Red(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Rr=Color1(2);
    Rg=Color1(3);
    Rb=Color1(4);
elseif Jmax==2
    Rr=Color2(2);
    Rg=Color2(3);
    Rb=Color2(4);
elseif Jmax==3
    Rr=Color3(2);
    Rg=Color3(3);
    Rb=Color3(4);
elseif Jmax==4
    Rr=Color4(2);
    Rg=Color4(3);
    Rb=Color4(4);
else Rr=Color5(2);
    Rg=Color5(3);
    Rb=Color5(4);
end;

%______________________________________________________________________
%Определение зеленого цвета в эталоне
%______________________________________________________________________

Green=[Color1g,Color2g,Color3g,Color4g,Color5g];

Gmax=0;
Jmax=0;
for i=1:5
    if Green(i)>Gmax
        Gmax=Green(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Gr=Color1(2);
    Gg=Color1(3);
    Gb=Color1(4);
elseif Jmax==2
    Gr=Color2(2);
    Gg=Color2(3);
    Gb=Color2(4);
elseif Jmax==3
    Gr=Color3(2);
    Gg=Color3(3);
    Gb=Color3(4);
elseif Jmax==4
    Gr=Color4(2);
    Gg=Color4(3);
    Gb=Color4(4);
else Gr=Color5(2);
    Gg=Color5(3);
    Gb=Color5(4);
end;

%______________________________________________________________________
%Определение синего цвета в эталоне
%______________________________________________________________________

Blue=[Color1b,Color2b,Color3b,Color4b,Color5b];

Bmax=0;
Jmax=0;
for i=1:5
    if Blue(i)>Bmax
        Bmax=Blue(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Br=Color1(2);
    Bg=Color1(3);
    Bb=Color1(4);
elseif Jmax==2
    Br=Color2(2);
    Bg=Color2(3);
    Bb=Color2(4);
elseif Jmax==3
    Br=Color3(2);
    Bg=Color3(3);
    Bb=Color3(4);
elseif Jmax==4
    Br=Color4(2);
    Bg=Color4(3);
    Bb=Color4(4);
else Br=Color5(2);
    Bg=Color5(3);
    Bb=Color5(4);
end;



%%Определение площади 1 пикселя
pixel1mm=(1/vector(1)+1/vector(2)+1/vector(3)+1/vector(4)+1/vector(5))/5;
squre1pixelmm=pixel1mm*pixel1mm;


%%Устранение эталона
pixbw=0;
while (pixbw~=1)
   xc=xc+1;
   pixbw=I(round(yc),round(xc));
end

xc=xc+5;
bwse=bwselect(I,xc,yc);
%figure, imshow(bwse)   
K=I;
fill=bwfill(bwse,'holes');
se=strel('diamond', 4);
fill=imdilate(fill, se);
%figure, imshow(fill) 
%grayimg = rgb2gray(img);
X=grayimg;
X(fill)=NaN;
grayimg=X;
I=grayimg;
%figure, imshow(I);
%%_____________________________________________________________________
%%Нахождение границ родинки
%%_____________________________________________________________________
BW1 = edge(I,'roberts',0.045);
%t=40; % Thres Hold value
%s = 55; % Seed Value
%f=I;
%[g, nr,si,ti] = regiongrow(f,s,t);
%figure,imshow(f),title('Input Image');
%figure,imshow(ti),title('After Segmentation');

figure, imshow(BW1)


se90=strel('line', 3, 90);
se0=strel('line', 3, 0);      
BWsdil=imdilate(BW1, [se90 se0]);
%figure, imshow(BWsdil), title('dilated gradient mask');
BWdfill=imfill(BWsdil, 'holes');
%figure, imshow(BWdfill); title('binary image with filled holes');
BWnobord=imclearborder(BWdfill, 8);
%figure, imshow(BWnobord), title('cleared border image');
seD=strel('diamond', 8);
BWfinal=imerode(BWnobord, seD);
BWfinal=imerode(BWfinal, seD);
%figure, imshow(BWfinal)
se1=strel('disk',4);
%BWf=imdilate(BWfinal,se1);

rodinkapo=BW1;
%rodinkapo=imclearborder(rodinkapo);
dist = bwdist(rodinkapo);
figure, imshow(dist,[])
cdata=imread('C:\Users\Елена\Desktop\Диплом\2519K.png');
bwtest=im2bw(cdata,eps);
bwtest=1-bwtest;
% figure, imshow(bwtest);
% figure, imshow(1-bwtest);
% fill=bwfill(1-bwtest, 'holes');
% figure, imshow(fill);

contur = im2bw(bwtest);
mean(dist(find(contur==1)))



%figure, imshow(BWfinal)
BWoutline=bwperim(BWfinal);
Segout=img;
BWf=imdilate(BWoutline,se1);
%figure, imshow(BWf)
Segout(BWf)=0; 
figure, imshow(Segout), title('Границы изображения');


%%Нахождение площади родинки
Sumpixels = sum(BWfinal(:));
squre1rodinki = squre1pixelmm*Sumpixels;
%display(squre1rodinki);