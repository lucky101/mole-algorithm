function Z = histTADAAApt(X)

%fonction d'adaptiation d'histogramme en fonction de sa variation dans un 
%sous intervalle
%entree: image
%sortie: image adaptee

X=im2double(X);
h = hist(X(:),256);%l'histo le plus detaille possible sur une image uint8
m = max(h);
h(h==0)=NaN;%certaines valeurs creent d'innutiles variations

%%1: calculer les nouvelles frontieres
oldBound = 0:25:254; % choisi arbitrairement de faire 10 intervalles
oldBound = [oldBound, 255];
tic
newBound = zeros(size(oldBound));
for i=1:numel(oldBound)-1 % calcul des nouveaux intervalles en fonction des
    ma = max(h(oldBound(i)+1:oldBound(i+1))); % variations dans les anciens
    mi = min(h(oldBound(i)+1:oldBound(i+1)));
    newBound(i+1) = newBound(i)+ 255*(ma-mi)/m;    
end
newBound = 255*newBound/newBound(end);
toc
tic
LUT = uint8(zeros(1,256));
for ind = 2:numel(oldBound) % adaptation lineaire des valeurs 
    b1 = oldBound(ind-1); %   sur les nouveaux intervalles
    c1 = oldBound(ind);
    b2 = newBound(ind-1);
    c2 = newBound(ind);
    m = (c2-b2)/(c1-b1);
    k = b2 - b1*m;
    M = (b1:c1-1);
    LUT(M+1) = round(m*M + k);
    clear M
end
LUT(256) = 255; % application de la table des nouveaux intervalles aux
Z = intlut(im2uint8(X), LUT); % valeurs de l'image
toc
