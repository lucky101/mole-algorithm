function tree_cyto=get_param_tree_cyto(X,Y)
%%get_param_tree_cyto.m
%this functions uses the image of the cell in RGB and SATURATION
%to retrieve into a line vector the texture parameters used for
%leukocyte classification
%   cyto_div_1, cyto_div_4, 


Y=Y(:,:,2);
Y=Y-min(min(Y));
Y=Y./max(max(Y));
cyto=SegmentCyto7(X,0);%is worth checkin if rgb2hsv is not redone here
%cyto=cyto-min(min(cyto));
%cyto=cyto./max(max(cyto));
vX=im2double(cyto).*Y;
%vX=vX-min(min(vX));
%vX=vX./max(max(vX));
%quad tree decomposition
quad=zeros(512,512);
quad(1:451,1:451)=vX;%substitute the values
%THE quad tree decomposition
S = qtdecomp(quad,mean(mean(quad)));%mean(mean(quad))=threshold

num_1=length(find(S==1));
num_4=length(find(S==4));
area=sum(sum(cyto));

%   cyto_div_1, cyto_div_4, 
tree_cyto=[num_1/area,num_4/area,area];