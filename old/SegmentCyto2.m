function cyto=SegmentCyto2(X);
%%this function segments the cytoplasm of leukocyte using local entropy
%(texture)
%recieves the image in RGB format
%supress the nucleus, using the tecniques developped for skeletization

Y=rgb2hsv(X);
S=Y(:,:,2);
V=Y(:,:,3);


%%search for variation
lap=del2(V);

%%entropy filter to see that cytoplasm is less random than surrounding
%milieu
J=entropyfilt(lap,true(9));

%%normalize between 0 and 1
J=J-min(min(J));
J=J/max(max(J));

%%create mask
bw=im2bw(J,mean(mean(J)));%0.3);
bw=not(bw);

%%opt: fill holes
fill=bwfill(bw, 'holes');

lab=bwlabel(fill);
prop=regionprops(lab);
are=[prop.Area];
a=find(are==max(are));
[a,b]=find(lab==a);
sel=bwselect(fill,b(1),a(1));


%[w,h]=size(fill);
%[a,b]=gravity_center(fill, w,h);
%sel=bwselect(fill,a,b);
%gravity center BW, w, h

%%nucleus supression
h=fspecial('gaussian',11,5);
blur=imfilter(S,h);

BW=im2bw(blur,0.4);

%%supress nucleus from cytoplasm
sel=sel-BW;
%sel=sel-min(min(sel));
%sel=sel./max(max(sel));
cyto=sel;
