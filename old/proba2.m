close all;
clear; 
%%Открытие исходного изображения
%A = imread('2519.JPG','jpg');
%imwrite(A,'O2519.bmp','bmp'); 
%img=imread('O2519.bmp');
img=imread('2519.bmp');
figure, imshow(img)
%%Нахождение R,G,B компонентов
R = img (:,:,1);
G = img (:,:,2);
B = img(:,:,3);

gray(:,:,1) = R;
gray(:,:,2) = G;
gray(:,:,3) = B;

gray1 = rgb2gray(gray);
%figure, imshow(gray1)
grayimg = rgb2gray(img);%%Преобразование в оттенки серого
%figure, imshow(grayimg)
grayimg = imadjust(grayimg);%%Улучшение качества изображения
%figure, imshow(grayimg)
%figure, imshow(grayimg)

%%----------------------------------------------------------------------
%%Нахождение эталона
%%----------------------------------------------------------------------
bw = edge(grayimg,'canny', 0.15, 10);
bw = imfill(bw,'holes');
se = strel('disk',1); 
bw = imopen(bw,se);
I=im2bw(grayimg,0.7);
%figure, imshow(I)
P=1-I;
%figure, imshow(P)
[A,L] = bwboundaries(bw);
stats = regionprops(L,'Centroid','EquivDiameter');
%figure, imshow(img)
hold on

vector=zeros(5,1);
coordinat=zeros(5,2);

for k = 1:length(A)
 boundary = A{k};
 radius = stats(k).EquivDiameter/2;
 if radius > 29 
 vector(k,1)=radius;
 
 xc = stats(k).Centroid(1);
 yc = stats(k).Centroid(2);
 
 coordinat(k,1)=xc;
 coordinat(k,2)=yc;
 
 theta = 0:0.01:2*pi;
 Xfit = radius*cos(theta) + xc;
 Yfit = radius*sin(theta) + yc;
 %plot(Xfit, Yfit, 'g');
 %text(boundary(1,2)-15,boundary(1,1)+15, num2str(radius,3),'Color','y',...
 % 'FontSize',8);
 end
end

%%Нахождение вектора радиусов(в пикселях) эталона
vector(vector==0)=[];
%display(vector);

%%Нахождение координат центров эталона
coordinat(coordinat==0)=[];
coordinat=reshape(coordinat,5,2);
%display(coordinat);

%--------------------------------------------------------------------
%Находим все 5 кругов эталона и их цветовые характеристики
%--------------------------------------------------------------------

%1-й круг
circle1x=coordinat(1,1);
circle1y=coordinat(1,2);
circle1=bwselect(P,circle1x,circle1y);
%figure, imshow(circle1) 
Color1r=mean(R(circle1(:)));
Color1g=mean(G(circle1(:)));
Color1b=mean(B(circle1(:)));
Color1=[1,Color1r,Color1g,Color1b];

%2-й круг
circle2x=coordinat(2,1);
circle2y=coordinat(2,2);
circle2=bwselect(P,circle2x,circle2y);
%figure, imshow(circle2) 
Color2r=mean(R(circle2(:)));
Color2g=mean(G(circle2(:)));
Color2b=mean(B(circle2(:)));
Color2=[2,Color2r,Color2g,Color2b];

%3-й круг
circle3x=coordinat(3,1);
circle3y=coordinat(3,2);
circle3=bwselect(P,circle3x,circle3y);
%figure, imshow(circle3) 
Color3r=mean(R(circle3(:)));
Color3g=mean(G(circle3(:)));
Color3b=mean(B(circle3(:)));
Color3=[3,Color3r,Color3g,Color3b];

%4-й круг
circle4x=coordinat(4,1);
circle4y=coordinat(4,2);
circle4=bwselect(P,circle4x,circle4y);
%figure, imshow(circle4) 
Color4r=mean(R(circle4(:)));
Color4g=mean(G(circle4(:)));
Color4b=mean(B(circle4(:)));
Color4=[4,Color4r,Color4g,Color4b];

%5-й круг
circle5x=coordinat(5,1);
circle5y=coordinat(5,2);
circle5=bwselect(P,circle5x,circle5y);
%figure, imshow(circle5) 
Color5r=mean(R(circle5(:)));
Color5g=mean(G(circle5(:)));
Color5b=mean(B(circle5(:)));
Color5=[5,Color5r,Color5g,Color5b];


%______________________________________________________________________
%Определение красного цвета в эталоне
%______________________________________________________________________
Red=[Color1r,Color2r,Color3r,Color4r,Color5r];

Rmax=0;
Jmax=0;
for i=1:5
    if Red(i)>Rmax
        Rmax=Red(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Rr=Color1(2);
    Rg=Color1(3);
    Rb=Color1(4);
elseif Jmax==2
    Rr=Color2(2);
    Rg=Color2(3);
    Rb=Color2(4);
elseif Jmax==3
    Rr=Color3(2);
    Rg=Color3(3);
    Rb=Color3(4);
elseif Jmax==4
    Rr=Color4(2);
    Rg=Color4(3);
    Rb=Color4(4);
else Rr=Color5(2);
    Rg=Color5(3);
    Rb=Color5(4);
end;

%______________________________________________________________________
%Определение зеленого цвета в эталоне
%______________________________________________________________________

Green=[Color1g,Color2g,Color3g,Color4g,Color5g];

Gmax=0;
Jmax=0;
for i=1:5
    if Green(i)>Gmax
        Gmax=Green(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Gr=Color1(2);
    Gg=Color1(3);
    Gb=Color1(4);
elseif Jmax==2
    Gr=Color2(2);
    Gg=Color2(3);
    Gb=Color2(4);
elseif Jmax==3
    Gr=Color3(2);
    Gg=Color3(3);
    Gb=Color3(4);
elseif Jmax==4
    Gr=Color4(2);
    Gg=Color4(3);
    Gb=Color4(4);
else Gr=Color5(2);
    Gg=Color5(3);
    Gb=Color5(4);
end;

%______________________________________________________________________
%Определение синего цвета в эталоне
%______________________________________________________________________

Blue=[Color1b,Color2b,Color3b,Color4b,Color5b];

Bmax=0;
Jmax=0;
for i=1:5
    if Blue(i)>Bmax
        Bmax=Blue(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Br=Color1(2);
    Bg=Color1(3);
    Bb=Color1(4);
elseif Jmax==2
    Br=Color2(2);
    Bg=Color2(3);
    Bb=Color2(4);
elseif Jmax==3
    Br=Color3(2);
    Bg=Color3(3);
    Bb=Color3(4);
elseif Jmax==4
    Br=Color4(2);
    Bg=Color4(3);
    Bb=Color4(4);
else Br=Color5(2);
    Bg=Color5(3);
    Bb=Color5(4);
end;



%%Определение площади 1 пикселя
pixel1mm=(1/vector(1)+1/vector(2)+1/vector(3)+1/vector(4)+1/vector(5))/5;
squre1pixelmm=pixel1mm*pixel1mm;


%%Устранение эталона
pixbw=0;
while (pixbw~=1)
   xc=xc+1;
   pixbw=I(round(yc),round(xc));
end

xc=xc+5;
bwse=bwselect(I,xc,yc);
%figure, imshow(bwse)   
K=I;
fill=bwfill(bwse,'holes');
se=strel('diamond', 4);
fill=imdilate(fill, se);
%figure, imshow(fill) 
%grayimg = rgb2gray(img);
X=grayimg;
X(fill)=NaN;
grayimg=X;
I=grayimg;
%figure, imshow(I);

%%_____________________________________________________________________
%%Нахождение границ родинки
%%_____________________________________________________________________

%BW1 = edge(I,'canny', 0.25, 30);
t=40; % Thres Hold value
s = 55; % Seed Value
f=I;
[g, nr,si,ti] = regiongrow(f,s,t);
%figure,imshow(f),%title('Input Image');
figure,imshow(~ti),%title('After Segmentation');


se90=strel('line', 3, 90);
se0=strel('line', 3, 0);      
BWsdil=imdilate(ti, [se90 se0]);
figure, imshow(~BWsdil), %title('dilated gradient mask');
BWdfill=imfill(BWsdil, 'holes');
figure, imshow(~BWdfill); %title('binary image with filled holes');
BWnobord=imclearborder(BWdfill, 8);
figure, imshow(~BWnobord), %title('cleared border image');
seD=strel('diamond', 7);
BWfinal=imerode(BWnobord, seD);
figure, imshow(~BWfinal)
BWfinal=imerode(BWfinal, seD);
figure, imshow(~BWfinal)
%subplot(1,3,2);
primitive = strel('disk', 38);
BWfinal = imdilate(BWfinal, primitive);
BWfinal = imerode(BWfinal, primitive);
figure, imshow(~BWfinal)
se1=strel('disk',4);
ttt
BWoutline=bwperim(BWfinal);
Segout=img;
BWf=imdilate(BWoutline,se1);
Segout(BWf)=0; 
figure, imshow(Segout)

%%Нахождение площади родинки
Sumpixels = sum(BWfinal(:));
squre1rodinki = squre1pixelmm*Sumpixels;
%display(squre1rodinki);

%----------------------------------------------------------------------
%Нахождение цветовых характеристик родинки
%----------------------------------------------------------------------

feats=regionprops(BWfinal,'Centroid');
Centroid=feats.Centroid;
xrod=Centroid(1);
yrod=Centroid(2);

rodinka = bwselect(BWfinal,xrod,yrod);
%figure, imshow(rodinka)
ss = bwlabel(BWfinal);
%figure, imshow(ss)
%нахождение диаметра родинки
findd  = regionprops(ss, 'MajorAxisLength');
dd = [findd.MajorAxisLength];
d = dd*pixel1mm;
display(d);


Nr=mean(R(rodinka(:)));
Ng=mean(G(rodinka(:)));
Nb=mean(B(rodinka(:)));
%--------------------------------------------------------------------
%нахождение формы
formac  = regionprops(ss, 'ConvexArea');
formae  = regionprops(ss, 'Eccentricity');

term = abs(Sumpixels-formac.ConvexArea);
formaex  = regionprops(ss, 'Extent');
formas  = regionprops(ss, 'Solidity');

%display(formae)
%display(term)
%display(formaex)
%display(formas)


if term>=5000
disp('произвольная форма')    
elseif formae.Eccentricity<=0.6000
disp('круглая форма')     
else disp('овальная форма')    
end;
 



 %ttt
 
% for n=1:L-4
%     In(n,:) = (I(n+3,:)-I(n,:))/3;
% end
% 
% for m=1:W-4
%     Im(:,m) = (I(:,m+3)-I(:,m))/3;
% end
% tempx = zeros(2553, 2601);
% tempy = zeros(2553, 2601);
% tempx(:, 1:2597) = Im;
% tempy(1:2549, :) = In;
% proiz = sqrt((tempx).^2+(tempy).^2);
% proiz = proiz - min(proiz(:));
% proiz = proiz./max(proiz(:));
% figure, imshow(proiz, [])



%-----------------------------------------------------------------------
%классификатор
%-----------------------------------------------------------------------

load('Color1.txt');
Color1=Color1';%due to easiness in file creation
y=Color1(1,:);%labels
x=Color1(2:end,:);%data

%%ununderstood parameters
labels               = unique(y);

options.method       = 7;
options.holding.rho  = 0.7;
options.holding.K    = 242;

options.weaklearner  = 0; %0->stump, 1->perceptron
options.epsi         = 0.1;%%perceptron
options.lambda       = 1e-2;%%perceptron
options.max_ite      = 1000;%%perceptron
options.T            = 50;%number of weak lerners

%%training itself
model_gentle=gentleboost_model(x,y,options.T,options);

%rgb=imread('2519.bmp');
rgb=img;
vectorClass=[Nr/Rr,Ng/Rg,Nb/Rb,Nr/Gr,Ng/Gg,Nb/Gb,Nr/Br,Ng/Bg,Nb/Bb];
vectorClass=vectorClass';
%%classify
[yTest,fxtrain]=gentleboost_predict(vector,model_gentle,options);

yTest=yTest+1;

if yTest==1
disp('цвет родинки-1') 
elseif yTest==2
disp('цвет родинки-2')  
elseif yTest==3; F = 2 ; 
disp('цвет родинки-оранжево-персиковый')   
elseif yTest==4
disp('цвет родинки-4') 
elseif yTest==5; 
disp('цвет родинки-5')    
elseif yTest==6; F = 1 ;
disp('цвет родинки-6')    
elseif yTest==7; F = 1 ;
disp('цвет родинки-7')    
elseif yTest==8; F = 1 ;    
disp('цвет родинки-8')    
elseif yTest==9 ; F = 1 ;   
disp('цвет родинки-9')   
elseif yTest==10; F = 1 ;
disp('цвет родинки-10')    
elseif yTest==11  ; F = 1 ;  
disp('цвет родинки-11')    
elseif yTest==12  ; F = 1 ;  
 disp('цвет родинки-12')   
elseif yTest==13; F = 1 ;
 disp('цвет родинки-13')   
elseif yTest==14; F = 1 ;    
 disp('цвет родинки-14')   
elseif yTest==15; F = 1 ;   
 disp('цвет родинки-15')   
elseif yTest==16; F = 1 ;
 disp('цвет родинки-16')   
elseif yTest==17; F = 1 ;
 disp('цвет родинки-17')   
elseif yTest==18; F = 1 ;    
 disp('цвет родинки-18')   
elseif yTest==19; F = 1 ;    
 disp('цвет родинки-19')   
elseif yTest==20; F = 1 ;    
 disp('цвет родинки-20')   
elseif yTest==21; F = 1 ;    
 disp('цвет родинки-21')   
elseif yTest==22; F = 1 ;    
 disp('цвет родинки-22')   
elseif yTest==23; F = 1 ;
 disp('цвет родинки-23')   
elseif yTest==24; F = 1 ;    
 disp('цвет родинки-24')   
elseif yTest==25; F = 1 ;    
 disp('цвет родинки-черный')       
else disp('цвет родинки-26')
end;


%T = table(['Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'],[]);
