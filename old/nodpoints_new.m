function d=nodpoints_new(N)
persistent lut
if isempty(lut)
    lut=makelut(@nodpoint_fcn,3);
end
d=applylut(N,lut);

function is_nod_point=nodpoint_fcn(nhood)
flag=1;
for ind1=1:2:3
    for ind2=1:2
        flag=flag*(~(nhood(ind1,ind2)*nhood(ind1,ind2+1)))*(~(nhood(ind2,ind1)*nhood(ind2+1,ind1)));
    end;
end;
is_nod_point=nhood(2,2)&(sum(nhood(:))>=4)&flag;