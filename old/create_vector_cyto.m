function vector=create_vector_cyto(X, train)
%%create_vector_cyto.m
%creates training set for multi-class adaboost, for leukocyte
%classification
%the vector takes the following form:
%{0 1 2 3 4} - the label, to be 0 in case of classification
%[label, 
%   ske_d_cyto, ske_Area, ske_Area/perimeter, ske_#min, ske_exc, 
%   nu_Ener, nu_Corr, nu_Cont, 
%   cyto_div_1, cyto_div_4, 
%   cyto_hom_70, cyto_con_70, cyto_ene_70];
%
%X is the image
%Y is the image, HSV, train is the label
%if train = 0, classification is assumed.

Y=rgb2hsv(X);

%divide ut regnas
skeleton=get_param_skeleton(Y(:,:,2));
tex_nuke=get_param_tex_nuke(Y(:,:,2));
tree_cyto=get_param_tree_cyto(X,Y);
tex_cyto=get_param_tex_cyto(X,Y);
cyto_val=get_cyto_euler_val(X);

if(train==0)%then classification
    %vector=double([skeleton, tex_nuke, tree_cyto, tex_cyto, cyto_val]);
    %vector=double([tree_cyto, tex_cyto, cyto_val]);
    vector=double([skeleton, tex_nuke, tree_cyto, tex_cyto, cyto_val]);
else%else training
    %vector=double([train, skeleton, tex_nuke, tree_cyto, tex_cyto, cyto_val]);
    %vector=double([train, tree_cyto, tex_cyto]);
    vector=double([train, skeleton, tex_nuke, tree_cyto, tex_cyto, cyto_val]);
end
%easy isn't it?
