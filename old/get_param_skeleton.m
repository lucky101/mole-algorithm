function skeleton=get_param_skeleton(Y)
%%get_param_skeleton.m
%this functions uses the image of the cell in SATURATION channel
%to retrieve into a line vector the skeleton parameters used for
%leukocyte classification
%   ske_d_cyto, ske_Area, ske_Area/perimeter, ske_#min, ske_exc, 

%%gausian filtering to improve edge smootheness
h=fspecial('gaussian',11,5);
blur=imfilter(Y,h);

BW=im2bw(blur,0.4);

%%topological filling-> NO HOLES!!!
fill=imfill(BW,'holes');

L=bwlabel(fill);
props=regionprops(L,'Area', 'Perimeter');

%%distance transform to get skeleton manually
%1. distance transformation
dist=bwdist(not(fill));
%2. edge finding using laplacian
lap=del2(dist);
lap=lap*100;
seuil=min(min(lap))*0.2;
for i=1:451
    for j=1:451
        if(lap(i,j)<seuil)
            BW4(i,j)=1;
        else
            BW4(i,j)=0;
        end
    end
end
%thinning helps finding minutiaes
BW4=bwmorph(BW4,'thin');

%3. get different regions
label=bwlabel(BW4);
stats=regionprops(label,'Area','Eccentricity');

for i=1:size(stats)
    a(i)=stats(i).Area;
    eck(i)=stats(i).Eccentricity;
end
%geometry: index, area, orientation, excentricity
a=[[1:size(a')];a;eck];%indexed regions =)
a=a';
a=sortrows(a,2);

%4. get the indexes of the 4 biggest regions
vec_regprops=a(end-4+1:end,:);
while(vec_regprops(1,2)<0.25*vec_regprops(end,2))%filter by size to eliminate remaining artefacts
    vec_regprops=vec_regprops(2:end,:);
end
index=vec_regprops(:,1);
r=size(index);

%5. get pixel indexes
for i=1:r(1)
    [l,c]=find(label==index(i));
    sl(i)=l(1);
    sc(i)=c(1);
end

%6. select regions finally
selected=bwselect(label,sc,sl);

%7. get minutiaes.
endpts=endpoints(selected);
d=nodpoints_new(selected);

clear a eck index;

%%parameters computation
%area<->perimeter
g=size(props);
ar=0;
per=0;
for i=1:g(1)
    ar=ar+props(i).Area;
    per=per+props(i).Perimeter;
end
surface=ar;
perim=per;

%minutiae_number
u=size(find(endpts==1));
v=size(find(d==1));
minutiae_number=u(1)+v(1);

%mean_distance_cyto
[r1,r2]=find(d==1);
if(isempty(r1))
    [r1,r2]=find(endpts==1);
end
eloignment=dist(r1,r2);
eloignment=eloignment.*eye(size(eloignment));
elo=mean(eloignment(find(eloignment~=0)));
mean_dist_cyto=elo;

%excentricity
excentricity=[NaN,NaN,NaN,NaN];
for k=1:r(1)
    excentricity(k)=vec_regprops(k,3);
end
ex=mean(excentricity(find(not(isnan(excentricity)))));

%   ske_d_cyto, ske_Area, ske_Area/perimeter, ske_#min, ske_exc,
skeleton=[mean_dist_cyto, surface, surface./perim, minutiae_number, ex];
