 function varargout = test(varargin)
% TEST MATLAB code for test.fig
%      TEST, by itself, creates a new TEST or raises the existing
%      singleton*.
%
%      H = TEST returns the handle to a new TEST or the handle to
%      the existing singleton*.
%
%      TEST('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEST.M with the given input arguments.
%
%      TEST('Property','Value',...) creates a new TEST or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before test_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to test_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help test

% Last Modified by GUIDE v2.5 07-Feb-2016 20:49:48

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @test_OpeningFcn, ...
                   'gui_OutputFcn',  @test_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


     
% --- Executes just before test is made visible.
function test_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to test (see VARARGIN)

% Choose default command line output for test
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes test wait for user response (see UIRESUME)
% uiwait(handles.figure);


% --- Outputs from this function are returned to the command line.
function varargout = test_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function [g, NR, SI, TI] = regiongrow(f, S, T)
%REGIONGROW Perform segmentation by region growing.
%   [G, NR, SI, TI] = REGIONGROW(F, SR, T).  S can be an array (the
%   same size as F) with a 1 at the coordinates of every seed point
%   and 0s elsewhere.  S can also be a single seed value. Similarly,
%   T can be an array (the same size as F) containing a threshold
%   value for each pixel in F. T can also be a scalar, in which
%   case it becomes a global threshold.   
%
%   On the output, G is the result of region growing, with each
%   region labeled by a different integer, NR is the number of
%   regions, SI is the final seed image used by the algorithm, and TI
%   is the image consisting of the pixels in F that satisfied the
%   threshold test. 

%   Copyright 2002-2004 R. C. Gonzalez, R. E. Woods, & S. L. Eddins
%   Digital Image Processing Using MATLAB, Prentice-Hall, 2004
%   $Revision: 1.4 $  $Date: 2003/10/26 22:35:37 $

f = double(f);
% If S is a scalar, obtain the seed image.
if numel(S) == 1
   SI = f == S;
   S1 = S;
else
   % S is an array. Eliminate duplicate, connected seed locations 
   % to reduce the number of loop executions in the following 
   % sections of code.
   SI = bwmorph(S, 'shrink', Inf);  
   J = find(SI);
   S1 = f(J); % Array of seed values.
end
 
TI = false(size(f));
for K = 1:length(S1)
   seedvalue = S1(K);
   S = abs(f - seedvalue) <= T;
   TI = TI | S;
end

% Use function imreconstruct with SI as the marker image to
% obtain the regions corresponding to each seed in S. Function
% bwlabel assigns a different integer to each connected region.
[g, NR] = bwlabel(imreconstruct(SI, TI));


% --- Executes on button press in open.
function open_Callback(hObject, eventdata, handles)
% hObject    handle to open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName, PathName] = uigetfile('*.bmp');
if FileName~=0
FullName = [PathName FileName];
img=imread(FullName);
image('CData', img)
axes(handles.axes11);
imshow(img); %title('Исходное изображение')


%guidata(h, handles);
  %%Нахождение R,G,B компонентов
R = im2double(img (:,:,1));
G = im2double(img (:,:,2));
B = im2double(img(:,:,3));

grayimg = rgb2gray(img);%%Преобразование в оттенки серого
grayimg = imadjust(grayimg);%%Улучшение качества изображения

%%----------------------------------------------------------------------
%%Нахождение эталона
%%----------------------------------------------------------------------
bw = edge(grayimg,'canny', 0.15, 10);
bw = imfill(bw,'holes');
se = strel('disk',1); 
bw = imopen(bw,se);
I=im2bw(grayimg,0.7);
%figure, imshow(I)
P=1-I;
%figure, imshow(P)
[A,L] = bwboundaries(bw);
stats = regionprops(L,'Centroid','EquivDiameter');
%set(handles.txtVverx,'String','Исходное изображение');
%axes(handles.axes6);
%imshow(img); 

hold on

vector=zeros(5,1);
coordinat=zeros(5,2);

for k = 1:length(A)
 boundary = A{k};
 radius = stats(k).EquivDiameter/2;
 if radius > 29 
 vector(k,1)=radius;
 
 xc = stats(k).Centroid(1);
 yc = stats(k).Centroid(2);
 
 coordinat(k,1)=xc;
 coordinat(k,2)=yc;
 
 theta = 0:0.01:2*pi;
 Xfit = radius*cos(theta) + xc;
 Yfit = radius*sin(theta) + yc;
 %plot(Xfit, Yfit, 'g');
 %text(boundary(1,2)-15,boundary(1,1)+15, num2str(radius,3),'Color','y',...
 % 'FontSize',8);
 end
end

%%Нахождение вектора радиусов(в пикселях) эталона
vector(vector==0)=[];
%display(vector);

%%Нахождение координат центров эталона
coordinat(coordinat==0)=[];
coordinat=reshape(coordinat,5,2);
%display(coordinat);

%--------------------------------------------------------------------
%Находим все 5 кругов эталона и их цветовые характеристики
%--------------------------------------------------------------------

%1-й круг
circle1x=coordinat(1,1);
circle1y=coordinat(1,2);
circle1=bwselect(P,circle1x,circle1y);
%figure, imshow(circle1) 
Color1r=mean(R(circle1(:)));
Color1g=mean(G(circle1(:)));
Color1b=mean(B(circle1(:)));
Color1=[1,Color1r,Color1g,Color1b];

%2-й круг
circle2x=coordinat(2,1);
circle2y=coordinat(2,2);
circle2=bwselect(P,circle2x,circle2y);
%figure, imshow(circle2) 
Color2r=mean(R(circle2(:)));
Color2g=mean(G(circle2(:)));
Color2b=mean(B(circle2(:)));
Color2=[2,Color2r,Color2g,Color2b];

%3-й круг
circle3x=coordinat(3,1);
circle3y=coordinat(3,2);
circle3=bwselect(P,circle3x,circle3y);
%figure, imshow(circle3) 
Color3r=mean(R(circle3(:)));
Color3g=mean(G(circle3(:)));
Color3b=mean(B(circle3(:)));
Color3=[3,Color3r,Color3g,Color3b];

%4-й круг
circle4x=coordinat(4,1);
circle4y=coordinat(4,2);
circle4=bwselect(P,circle4x,circle4y);
%figure, imshow(circle4) 
Color4r=mean(R(circle4(:)));
Color4g=mean(G(circle4(:)));
Color4b=mean(B(circle4(:)));
Color4=[4,Color4r,Color4g,Color4b];

%5-й круг
circle5x=coordinat(5,1);
circle5y=coordinat(5,2);
circle5=bwselect(P,circle5x,circle5y);
%figure, imshow(circle5) 
Color5r=mean(R(circle5(:)));
Color5g=mean(G(circle5(:)));
Color5b=mean(B(circle5(:)));
Color5=[5,Color5r,Color5g,Color5b];


%______________________________________________________________________
%Определение красного цвета в эталоне
%______________________________________________________________________
Red=[Color1r,Color2r,Color3r,Color4r,Color5r];

Rmax=0;
Jmax=0;
for i=1:5
    if Red(i)>Rmax
        Rmax=Red(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Rr=Color1(2);
    Rg=Color1(3);
    Rb=Color1(4);
elseif Jmax==2
    Rr=Color2(2);
    Rg=Color2(3);
    Rb=Color2(4);
elseif Jmax==3
    Rr=Color3(2);
    Rg=Color3(3);
    Rb=Color3(4);
elseif Jmax==4
    Rr=Color4(2);
    Rg=Color4(3);
    Rb=Color4(4);
else Rr=Color5(2);
    Rg=Color5(3);
    Rb=Color5(4);
end;

%______________________________________________________________________
%Определение зеленого цвета в эталоне
%______________________________________________________________________

Green=[Color1g,Color2g,Color3g,Color4g,Color5g];

Gmax=0;
Jmax=0;
for i=1:5
    if Green(i)>Gmax
        Gmax=Green(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Gr=Color1(2);
    Gg=Color1(3);
    Gb=Color1(4);
elseif Jmax==2
    Gr=Color2(2);
    Gg=Color2(3);
    Gb=Color2(4);
elseif Jmax==3
    Gr=Color3(2);
    Gg=Color3(3);
    Gb=Color3(4);
elseif Jmax==4
    Gr=Color4(2);
    Gg=Color4(3);
    Gb=Color4(4);
else Gr=Color5(2);
    Gg=Color5(3);
    Gb=Color5(4);
end;

%______________________________________________________________________
%Определение синего цвета в эталоне
%______________________________________________________________________

Blue=[Color1b,Color2b,Color3b,Color4b,Color5b];

Bmax=0;
Jmax=0;
for i=1:5
    if Blue(i)>Bmax
        Bmax=Blue(i);
        Jmax=i;
    end;
end
  
 
if Jmax==1
    Br=Color1(2);
    Bg=Color1(3);
    Bb=Color1(4);
elseif Jmax==2
    Br=Color2(2);
    Bg=Color2(3);
    Bb=Color2(4);
elseif Jmax==3
    Br=Color3(2);
    Bg=Color3(3);
    Bb=Color3(4);
elseif Jmax==4
    Br=Color4(2);
    Bg=Color4(3);
    Bb=Color4(4);
else Br=Color5(2);
    Bg=Color5(3);
    Bb=Color5(4);
end;



%%Определение площади 1 пикселя
pixel1mm=(1/vector(1)+1/vector(2)+1/vector(3)+1/vector(4)+1/vector(5))/5;
squre1pixelmm=pixel1mm*pixel1mm;


%%Устранение эталона
pixbw=0;
while (pixbw~=1)
   xc=xc+1;
   pixbw=I(round(yc),round(xc));
end

xc=xc+5;
bwse=bwselect(I,xc,yc);
%figure, imshow(bwse)   
K=I;
fill=bwfill(bwse,'holes');
se=strel('diamond', 4);
fill=imdilate(fill, se);
%figure, imshow(fill) 
%grayimg = rgb2gray(img);
X=grayimg;
X(fill)=NaN;
grayimg=X;
I=grayimg;

%%_____________________________________________________________________
%%Нахождение границ родинки
%%_____________________________________________________________________

%BW1 = edge(I,'canny', 0.25, 30);
t=45; % Thres Hold value
s = 55; % Seed Value
f=I;
[g, nr,si,ti] = regiongrow(f,s,t);
%figure,imshow(f),title('Input Image');
%figure,imshow(ti),title('After Segmentation');
se90=strel('line', 3, 90);
se0=strel('line', 3, 0);      
BWsdil=imdilate(ti, [se90 se0]);
%figure, imshow(BWsdil), title('dilated gradient mask');
BWdfill=imfill(BWsdil, 'holes');
%figure, imshow(BWdfill); title('binary image with filled holes');
BWnobord=imclearborder(BWdfill, 8);
%figure, imshow(BWnobord), title('cleared border image');
seD=strel('diamond', 7);
BWfinal=imerode(BWnobord, seD);
BWfinal=imerode(BWfinal, seD);
%figure, imshow(BWfinal)
%subplot(1,3,2);
se1=strel('disk',4);
primitive = strel('disk', 38);
BWfinal = imdilate(BWfinal, primitive);
BWfinal = imerode(BWfinal, primitive);
%set(handles.txtVniz,'String','Границы изображения');
axes(handles.axes3);
imshow(BWfinal)

BWoutline=bwperim(BWfinal);
Segout=img;
BWf=imdilate(BWoutline,se1);
Segout(BWf)=0; 
%subplot(1,3,3);
set(handles.txtVverx,'String','Результаты обработки');
axes(handles.axes6);
imshow(Segout)


%%Нахождение площади родинки
Sumpixels = sum(BWfinal(:));
squre1rodinki = squre1pixelmm*Sumpixels;
%display(squre1rodinki);
squre1rodinki=roundn(squre1rodinki,-2);

mm='мм^2';
squre=num2str(squre1rodinki);
%squre1=[squre ' ' mm];
set(handles.txtER,'string',squre);
%----------------------------------------------------------------------
%Нахождение цветовых характеристик родинки
%----------------------------------------------------------------------

feats=regionprops(BWfinal,'Centroid');
Centroid=feats.Centroid;
xrod=Centroid(1);
yrod=Centroid(2);

rodinka = bwselect(BWfinal,xrod,yrod);
%figure, imshow(rodinka)

%нахождение мах диаметра родинки
ss = bwlabel(BWfinal);
findd  = regionprops(ss, 'MajorAxisLength');
dd = [findd.MajorAxisLength];
d = dd*pixel1mm;
d=roundn(d,-2);

mm1='мм';
maxdiametr=num2str(d);
%maxdiametr1=[maxdiametr ' ' mm1];
set(handles.txtERR,'string',maxdiametr);


%нахождение мин диаметра родинки
% ss = bwlabel(BWfinal);
findds  = regionprops(ss, 'MinorAxisLength');
dds = [findds.MinorAxisLength];
ds = dds*pixel1mm;
ds=roundn(ds,-2);

mm1='мм';
mindiametr=num2str(ds);
%maxdiametr1=[maxdiametr ' ' mm1];
set(handles.textFF,'string',mindiametr);

Nr=mean(R(rodinka(:)));
Ng=mean(G(rodinka(:)));
Nb=mean(B(rodinka(:)));

%нахождение формы
formac  = regionprops(ss, 'ConvexArea');
formae  = regionprops(ss, 'Eccentricity');

term = abs(Sumpixels-formac.ConvexArea);
formaex  = regionprops(ss, 'Extent');
formas  = regionprops(ss, 'Solidity');

%display(formae)
%display(term)
%display(formaex)
%display(formas)


if term>=5000
%disp('произвольная форма')   
set(handles.txtTT,'String','произвольная')
elseif formae.Eccentricity<=0.6000
%disp('круглая форма')
set(handles.txtTT,'String','круглая')
else %disp('овальная форма') 
set(handles.txtTT,'String','овальная')  
end;

%Нахождение резкости границ

%[a,b]=find(BWoutline);

%wer = size(a,1)/36;
%wer = round(wer);

%kuu = zeros(wer,1);
%for n=1:36:size(a,1)
%xg=a(n);
%yg=b(n);
%xp = round(xrod)+round(1.1*(xrod-xg));
%yp = round(yrod)+round(1.1*(yrod-yg));
%profile2 = improfile(I,[yrod,xrod],[yg,xg]);
%profile2 = improfile(I,[xg,xrod],[yg,yrod]);
%profile2 = improfile(I,[yrod,yg],[xrod,xg]);
%profile = improfile(I,[yrod,yp],[xrod,xp]);

%pr = profile - profile2;
%profile = medfilt1(pr,11);
%proiz = diff(pr,2);
%ter = find(profile2==I(xg-xrod,yg-xrod));

%profile = medfilt1(profile,11);
%proiz = diff(profile,2);

%tex =find(profile==I(xg,yg));
%ter = size(profile2);
%texx = proiz(max(ter));
%kuu(n)= texx;
%end
rez = 1;
nerez = 0;
rewerew = 'резкая';
set(handles.textW,'String','резкая')

%-----------------------------------------------------------------------
%классификатор
%-----------------------------------------------------------------------

load('Color1.txt');
Color1=Color1';%due to easiness in file creation
y=Color1(1,:);%labels
x=Color1(2:end,:);%data

%%ununderstood parameters
labels               = unique(y);

options.method       = 7;
options.holding.rho  = 0.7;
options.holding.K    = 242;

options.weaklearner  = 0; %0->stump, 1->perceptron
options.epsi         = 0.1;%%perceptron
options.lambda       = 1e-2;%%perceptron
options.max_ite      = 1000;%%perceptron
options.T            = 50;%number of weak lerners

%%training itself
model_gentle=gentleboost_model(x,y,options.T,options);

rgb=img;

vectorClass=[Nr/Rr,Ng/Rg,Nb/Rb,Nr/Gr,Ng/Gg,Nb/Gb,Nr/Br,Ng/Bg,Nb/Bb];
vectorClass=vectorClass';
%%classify
[yTest,fxtrain]=gentleboost_predict(vector,model_gentle,options);

yTest=yTest+1;

if yTest==1
%disp('цвет родинки-1')

set(handles.txtWin,'String','светло-коричневый') 
elseif yTest==2 
%disp('цвет родинки-2') 
set(handles.txtWin,'String','светло-коричневый') 
elseif yTest==3 
%disp('цвет родинки-оранжево-персиковый')  
set(handles.txtWin,'String','светло-коричневый') 
elseif yTest==4 
%disp('цвет родинки-4') 
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==5
%disp('цвет родинки-5') 
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==6
%disp('цвет родинки-6')
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==7 
%disp('цвет родинки-7')    
set(handles.txtWin,'String','коричневый')
elseif yTest==8    
%disp('цвет родинки-8')    
set(handles.txtWin,'String','коричневый')
elseif yTest==9     
%disp('цвет родинки-9')   
set(handles.txtWin,'String','коричневый')
elseif yTest==10
%disp('цвет родинки-10')    
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==11    
%disp('цвет родинки-11')    
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==12    
 %disp('цвет родинки-12')   
set(handles.txtWin,'String','оливковый коричневый')
elseif yTest==13 
 %disp('цвет родинки-13')   
set(handles.txtWin,'String','темно-коричневый')
elseif yTest==14    
 %disp('цвет родинки-14')   
set(handles.txtWin,'String','красно-коричневый')
elseif yTest==15   
 %disp('цвет родинки-15')   
set(handles.txtWin,'String','темно-коричневый')
elseif yTest==16
 %disp('цвет родинки-16')   
set(handles.txtWin,'String','темно-коричневый')
elseif yTest==17
 %disp('цвет родинки-17')   
set(handles.txtWin,'String','темно-коричневый')
elseif yTest==18  
 %disp('цвет родинки-18')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==19    
 %disp('цвет родинки-19')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==20    
 %disp('цвет родинки-20')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==21   
 %disp('цвет родинки-21')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==22    
 %disp('цвет родинки-22')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==23
 %disp('цвет родинки-23')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==24    
% disp('цвет родинки-24')   
set(handles.txtWin,'String','светло-коричневый')
elseif yTest==25    
 %disp('цвет родинки-черный')       
set(handles.txtWin,'String','черный')
else %disp('цвет родинки-26'); 
set(handles.txtWin,'String','темно-коричневый')    
end;

%оттенки цветов-интенсивности пигментации
if yTest==1
%disp('цвет родинки-1') 
set(handles.txtIP,'String','светлая') 
elseif yTest==2
%disp('цвет родинки-2') 
set(handles.txtIP,'String','светлая') 
elseif yTest==3
%disp('цвет родинки-оранжево-персиковый')  
set(handles.txtIP,'String','светлая') 
elseif yTest==4
%disp('цвет родинки-4') 
set(handles.txtIP,'String','светлая')
elseif yTest==5
%disp('цвет родинки-5') 
set(handles.txtIP,'String','светлая')
elseif yTest==6
%disp('цвет родинки-6')
set(handles.txtIP,'String','светлая')
elseif yTest==7
%disp('цвет родинки-7')    
set(handles.txtIP,'String','темная')
elseif yTest==8    
%disp('цвет родинки-8')    
set(handles.txtIP,'String','темная')
elseif yTest==9    
%disp('цвет родинки-9')   
set(handles.txtIP,'String','темная')
elseif yTest==10
%disp('цвет родинки-10')    
set(handles.txtIP,'String','светлая')
elseif yTest==11    
%disp('цвет родинки-11')    
set(handles.txtIP,'String','светлая')
elseif yTest==12    
 %disp('цвет родинки-12')   
set(handles.txtIP,'String','темная')
elseif yTest==13
 %disp('цвет родинки-13')   
set(handles.txtIP,'String','очень темная')
elseif yTest==14    
 %disp('цвет родинки-14')   
set(handles.txtIP,'String','очень темная')
elseif yTest==15   
 %disp('цвет родинки-15')   
set(handles.txtIP,'String','очень темная')
elseif yTest==16
 %disp('цвет родинки-16')   
set(handles.txtIP,'String','очень темная')
elseif yTest==17
 %disp('цвет родинки-17')   
set(handles.txtIP,'String','очень темная')
elseif yTest==18    
 %disp('цвет родинки-18')   
set(handles.txtIP,'String','светлая')
elseif yTest==19    
 %disp('цвет родинки-19')   
set(handles.txtIP,'String','светлая')
elseif yTest==20    
 %disp('цвет родинки-20')   
set(handles.txtIP,'String','светлая')
elseif yTest==21    
 %disp('цвет родинки-21')   
set(handles.txtIP,'String','светлая')
elseif yTest==22    
 %disp('цвет родинки-22')   
set(handles.txtIP,'String','светлая')
elseif yTest==23
 %disp('цвет родинки-23')   
set(handles.txtIP,'String','светлая')
elseif yTest==24    
% disp('цвет родинки-24')   
set(handles.txtIP,'String','светлая')
elseif yTest==25    
 %disp('цвет родинки-черный')       
set(handles.txtIP,'String','очень темная')
else %disp('цвет родинки-26')
set(handles.txtIP,'String','очень темная')    
end;
%if 
 


%Определение вероятности малигнизации
if yTest==1;F = 1 ;
elseif yTest==2;F = 1 ;
elseif yTest==3; F = 1 ; 
elseif yTest==4;F = 1 ;
elseif yTest==5; F = 1 ;   
elseif yTest==6; F = 1 ;
elseif yTest==7; F = 2 ;
elseif yTest==8; F = 2 ;    
elseif yTest==9 ; F = 2 ;   
elseif yTest==10; F = 1 ;
elseif yTest==11; F = 1 ;  
elseif yTest==12; F = 4 ;  
elseif yTest==13; F = 3 ;
elseif yTest==14; F = 5 ;    
elseif yTest==15; F = 3 ;   
elseif yTest==16; F = 3 ;
elseif yTest==17; F = 3 ;
elseif yTest==18; F = 1 ;        
elseif yTest==19; F = 1 ;    
elseif yTest==20; F = 1 ;    
elseif yTest==21; F = 1 ;    
elseif yTest==22; F = 1 ;    
elseif yTest==23; F = 1 ;
elseif yTest==24; F = 1 ;    
elseif yTest==25; F = 6 ;    
else F = 3 ;
end;

if term>=5000
forma = 2;
elseif formae.Eccentricity<=0.6000
forma = 0;
else forma = 1;  
end;

if forma == 0;
formaprint = 'круглая';
elseif forma == 1;
formaprint = 'овальная';
else forma == 2; 
formaprint = 'произвольная';
end;


if d<=2;
    diametr = 1;
elseif d>2 & d<=4;
    diametr = 2;
elseif d>4 & d<=6;
    diametr = 3;
elseif d>6 & d<=8;
    diametr = 4;
elseif d>8 & d<=10;
    diametr = 5;
else d>10;
    diametr = 6;
end;

if squre1rodinki<=3.14;
    squrep = 1;
elseif squre1rodinki>3.14 & squre1rodinki<=12.56;
    squrep = 2;
elseif squre1rodinki>12.56 & squre1rodinki<=28.26;
    squrep = 3;
elseif squre1rodinki>28.26 & squre1rodinki<=50.24;
    squrep = 4;
elseif squre1rodinki>50.24 & squre1rodinki<=78.5;
    squrep = 5;
else squre1rodinki>78.5;
    squrep = 6;
end;
M = 1.3*forma+0.1*rez+0.5*F+0.5*diametr+0.5*squrep;
P = (M/11.7)*100;
P = roundn(P,0);
Pvermalig=num2str(P);
%maxdiametr1=[maxdiametr ' ' mm1];
set(handles.textPA,'string',Pvermalig);


TT = table([forma;rez;d;ds;squre1rodinki;yTest;yTest;P], 'VariableNames', {'ClinicParametres'}, 'RowNames',{'Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'});
%TT = table(['Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'],[forma;rez;d;ds;squre1rodinki;yTest;yTest;P]);
%disp(TT);
writetable(TT,'myData.xls','Range','B1:B9');
% TabT = table(['Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'],[35;70;d;d1;squre1rodinki;yTest;yTest;P]);
% %writetable(T,'myData.xls','Sheet',2,'Range','B2:F6');
%TTT = table(['Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации']);
%disp(TT);
%writetable(TT,'myData1.xls','Range','A1:A9');
%TT = table([forma;rez;d;ds;squre1rodinki;yTest;yTest;P], 'VariableNames', {'ClinicParametres'}, 'RowNames',{'Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'});

%TT = table(['Форма родинки';'Резкость границы';'Максимальный диаметр';'Минимальный диаметр';'Площадь родинки';'Цвет родинки';'Интенсивоность пигментации';'Вероятность малигнизации'],[forma;rez;d;ds;squre1rodinki;yTest;yTest;P], 'VariableNames', {'ClinicParametres'});
end


% --- Executes on button press in close.
function close_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close(ancestor(hObject,'figure'))


% --- Executes on button press in pushzoom.
function pushzoom_Callback(hObject, eventdata, handles)
% hObject    handle to pushzoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
zoom on;


% --- Executes on button press in pushClear.
function pushClear_Callback(hObject, eventdata, handles)
% hObject    handle to pushClear (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes11);
cla;
axes(handles.axes3);
cla;
axes(handles.axes6);
cla;

set(handles.txtER, 'String', '');
set(handles.txtWin, 'String', '');
set(handles.txtERR, 'String', '');
set(handles.txtTT, 'String', '');
set(handles.textFF, 'String', '');
set(handles.txtIP, 'String', '');
set(handles.textPA, 'String', '');
set(handles.textW, 'String', '');
