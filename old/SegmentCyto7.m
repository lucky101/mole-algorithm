function cyto=SegmentCyto7(X,nuc);
%%SegmentCyto7
%finds cytoplasm on the basis of edge finding and topology

Y=rgb2hsv(X);
S=Y(:,:,2);
V=Y(:,:,3);
%contrast
V=V-min(min(V));
V=V./max(max(V));

[w,h]=size(V);

%%nucleus
a=5;
kern=fspecial('gaussian',2*a,a);
blur=imfilter(S,kern);
nuke=im2bw(blur,0.4);

%%canny edge finding
a=5;
kern=fspecial('gaussian',2*a,a);
blur=imfilter(V,kern);
ed=edge(blur,'canny');
%take out the bluring on the image sides
ed(1:a,:)=0;
ed(end-a:end,:)=0;
ed(:,1:a)=0;
ed(:,end-a:end)=0;

%%sobel edge finding
sob=edge(V,'sobel');
%morphology, thanks Ksiucha!
se=strel('disk',2);
clo=imclose(sob,se);
se=strel('disk',4);
clo4=imclose(clo,se);
%without repport
dinuke=imdilate(nuke,se);

%%unite all the boundaries
ou=or(ed,clo4);
closou=imclose(ou,se);
se=strel('disk',1);
closou=imdilate(closou,se);

%%mass entropy
lap=del2(V);
%%entropy filter to see that cytoplasm is less random than surrounding
%milieu
J=entropyfilt(lap,true(9));
%%normalize between 0 and 1
J=J-min(min(J));
J=J/max(max(J));
%create mask
bw=im2bw(J,1.25*mean(mean(J)));

entrou=not(or(and(closou,not(dinuke)),bw));

%%unite entropy and boundaries
%entrou=or(bw,closou);
%entrou=not(entrou);
%entrou=imerode(entrou,se);
%cyto=entrou;

%%SEL
[xg,yg]=gravity_center(nuke,w,h);
while(~entrou(yg,xg))
    xg=xg+1;
end
sel=bwselect(entrou,xg,yg,4);
se=strel('disk',2);
clo2=imclose(sel,se);
se=strel('disk',4);
clo4=imclose(clo2,se);
fill=bwfill(clo4,'holes');
ope=imopen(fill,se);
% se=imdilate(sel,se);
% 
% %%glue the nucleus
% sel=or(sel,nuke);
% 
% %%finish
% se=strel('disk',4);
% fill=imclose(sel,se);
% fill=bwfill(fill,'holes');
% 
if(nuc==1)
    cyto=and(ope, not(nuke));
else
    cyto=ope;
end