function result=get_cyto_euler_val(X)
%%gets the mean value channels as well as the
%euler number of the cytoplasm:
%el=#objects-#holes

Y=rgb2hsv(X);
X=im2double(X);
cyto=SegmentCyto7(X,1);

X(:,:,1)=im2double(cyto).*X(:,:,1);
X(:,:,2)=im2double(cyto).*X(:,:,2);
X(:,:,3)=im2double(cyto).*X(:,:,3);
Y(:,:,1)=im2double(cyto).*Y(:,:,1);
Y(:,:,2)=im2double(cyto).*Y(:,:,2);
Y(:,:,3)=im2double(cyto).*Y(:,:,3);


R=mean(mean(X(:,:,1)));
G=mean(mean(X(:,:,2)));
B=mean(mean(X(:,:,3)));

H=mean(mean(Y(:,:,1)));
S=mean(mean(Y(:,:,2)));
V=mean(mean(Y(:,:,3)));

morph=bwmorph(cyto,'dilate',15);
el=bweuler(morph);

%result=[el,R,G,B,H,S,V];
%experimentally the H and S are the most significant to separate Eo and Neu
result=[el,H,S];