%%ikeda.m
%ikeda chaotic attractor

clear all
close all

%maxiter
N=200;

%max u
MU=1;
res=100;

traj=zeros(N+1, 2, res*MU+1);

for u=0.01:1/res:MU
    
    %initial conditions
    x=0;
    y=0;

    save=[x,y];


    for n=1:N
    
        t = 0.4 - 6/(1 + x^2 + y^2);
        x1 = 1 + u*(x*cos(t) - y*sin(t)) ;
        y1 = u*(x*sin(t) + y*cos(t)) ;
        x = x1;
        y = y1;

        save=[save;x,y];
        
    end
    traj(:,:,round(res*u))=save;
end