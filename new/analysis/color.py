import os
import json

import cv2 as cv

from utils.color import img_mean_color
from utils.debug import _debug_print
from .classifier import Classifier


FILE_DIR = os.path.dirname(os.path.realpath(__file__))
CLASSIFIER_FILES = {
    'classifier': os.path.join(FILE_DIR, 'trained.json'),
    'out_descr': os.path.join(FILE_DIR, 'classifier_results.json')
}

def train_classifier(filename):
    """ (Depracted - see load_classifier) Create classifier object and train it """

    # Read file lines
    with open(filename) as f:
        lines = f.readlines()

    # x - data, y - labels
    x, y = [], []

    # Split file data into labels (first column) and data (other columns)
    for line in lines:
        label, *data = line.split()
        y.append(label)
        x.append(data)

    # Create classifier object
    classifier = Classifier(learners_count=50)

    # Load data to classifier
    classifier.train(x, y)

    return classifier

def load_classifier(filename):
    """ Load classifier object """

    # Create classifier object
    classifier = Classifier(learners_count=50, labels_count=25)

    # Load classifier data from file
    classifier.load(filename)

    return classifier

def predict_to_result(predict):
    """ Convert predict (class index) to result data """

    with open(CLASSIFIER_FILES['out_descr'], encoding='utf-8') as f:
        results_match = json.load(f) 

    try:
        result = results_match[str(predict)] 
    except KeyError:
        raise Exception(f'Unexpected classifier result: {predict}. All possible outputs are listed in {CLASSIFIER_FILES["out_descr"]}')

    color, intensity, malignization_prob = result['color'], result['intensity'], result['malignization_prob']

    return color, intensity, malignization_prob

def get_color_consistency(img):
    return 1 # TODO

def analyse(rgb_colors, mole_img):
    """
    Analyse mole color (relative to rgb references) using classifier 
    
    Input: 
      rgb_colors - Array of 3 rgb tuples for true RGB colors on the image 
                   (e.g. [(), (), ()])
      mole_img - Mole image

    Return:
      Dict with fields: 
        color - name of the mole's color (in russian), e.g. "коричневый"
        intensity - name of mole's intensity (in russian), e.g. "светлая"
        malignization_prob - probability of malignization for the mole (int from 1 to 6)
    """

    hsv = cv.cvtColor(mole_img, cv.COLOR_BGR2HSV)
    avg_h, avg_s, avg_v = img_mean_color(hsv)

    _debug_print(f'Average color: H={round(avg_h, 1)} S={round(avg_s, 1)} V={round(avg_v, 1)}')

    if avg_h > 100 and 180 > avg_s > 125:
        if avg_v > 11.9:
            color, intensity, malignization_prob = 'светло-коричневый', 'светлая', 1
        else:
            color, intensity, malignization_prob = 'коричневый', 'темная', 2
    elif avg_v < 17 and 70 < avg_s < 130:
        if avg_h < 50 or avg_h > 60:
            color, intensity, malignization_prob = 'черный', 'очень темная', 8
        else:
            color, intensity, malignization_prob = 'желтый', 'очень темная', 4
    elif avg_s >= 130 and avg_v < 20 and 60 < avg_h < 120:
        color, intensity, malignization_prob = 'темно-коричневый', 'очень темная', 3  
    elif 30 < avg_s < 100 and avg_v >= 17:
        color, intensity, malignization_prob = 'серый', 'светлая', 5
    elif 200 > avg_h > 160:
        color, intensity, malignization_prob = 'голубой', 'светлая', 6 
    elif 130 < avg_h < 170 and ((avg_s > 100 and avg_v > 20) or (avg_s > 170 and avg_v < 10)): 
        color, intensity, malignization_prob = 'розовый', 'светлая', 7
    else:
        assert False, 'Uknnown color'

    return {
        'color': color, 
        'intensity': intensity, 
        'malignization_prob': malignization_prob
    }