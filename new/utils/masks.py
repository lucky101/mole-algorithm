import cv2 as cv
import numpy as np

def apply_circle_mask(img, x, y, r):
    h, w, c = img.shape
    mask = np.zeros((h, w))
    cv.circle(mask, (x, y), r, 1, thickness=-1)
    
    masked = roi * mask

    return masked