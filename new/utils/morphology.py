import cv2 as cv
import numpy as np

def imclearborder(img, pixels):
    h, w, *_ = img.shape
    
    img[0:pixels, 0:h] = 0
    img[0:w, 0:pixels] = 0
    img[w-pixels:w, 0:h] = 0
    img[0:w, h-pixels:h] = 0

    return img

def imfill_holes(img): 
    #th, im_th = cv.threshold(img, 220, 255, cv.THRESH_BINARY_INV);
    im_th = img
    im_floodfill = im_th.copy()
    
    h, w = im_th.shape[:2]
    mask = np.zeros((h+2, w+2), np.uint8)

    cv.floodFill(im_floodfill, mask, (0, 0), 255)
    im_floodfill_inv = cv.bitwise_not(im_floodfill)
    
    im_out = im_th | im_floodfill_inv
    
    return im_out