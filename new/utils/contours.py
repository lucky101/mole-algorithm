import cv2 as cv
import numpy as np


def get_min_maj_axis(contour):
    center, axes, orientation = cv.fitEllipse(contour)
    major_axis_len, minor_axis_len = max(axes), min(axes)

    return major_axis_len, minor_axis_len    

def get_eccentricy(contour):
    min_axis_len, maj_axis_len = get_min_maj_axis(contour)
    eccentricy = np.sqrt(1 - (min_axis_len/maj_axis_len)**2)

    return eccentricy

def get_equiv_diameter(contour):
    area = cv.contourArea(contour)
    equi_diameter = np.sqrt(4*area/np.pi)

    return equi_diameter

def draw_contours(img, contours):
    draw_img = img.copy()
    if len(draw_img.shape) == 2: # Grayscale image
        draw_img = cv.cvtColor(draw_img, cv.COLOR_GRAY2BGR)

    cv.drawContours(draw_img, contours, -1, (255,255,0), 3)

    return draw_img