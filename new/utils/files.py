import os
import uuid

import cv2 as cv

def save_report_image(img, folder="media", img_extension="png", suffix=""):
    """
    Insurres that the report folder is created (or creates it) and saves the image with 
    random unique name 
    """

    dir_path = os.path.dirname(os.path.realpath(__file__))
    save_to = os.path.join(dir_path, folder)

    if not os.path.exists(save_to):
        os.makedirs(save_to)

    filename = str(uuid.uuid4()) + suffix + '.' + img_extension
    filepath = os.path.join(save_to, filename)

    cv.imwrite(filepath, img)

    return filepath