import os

import cv2 as cv
import matplotlib.pyplot as plt


def only_if_env_variable(variable_name):
    """
      Allows decorated function execution only in case if specified env variable is setted
    """

    assert isinstance(variable_name, str)

    def wrapper(f):
      is_var = os.environ.get(variable_name)

      if is_var:
        return f
      else:
        return lambda *args, **kw : None
    
    return wrapper


def get_screen_size_windows():
    """ Returns display size (windows only) """

    import ctypes
    user32 = ctypes.windll.user32
    screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

    return screensize

def get_screen_size():
    """ Crossplatform gets display size """

    if os.name == 'nt': #Windows
        return get_screen_size_windows()
    else:
        raise NotImplementedError('Screen resolution getter isn\'t implemented for non Windows platforms')

@only_if_env_variable('DEBUG_IMSHOW')
def _debug_imshow(window_name, img):
    """
    After image showed - the window pauses and waits until any key will be pressed

    To set environment variable on Windows: 
      set DEBUG_IMSHOW=1
      ... run the code (e.g. python test.py) ...
    To set environment variable on Linpux/macOS:
      export DEBUG_IMSHOW=1
      ... run the code (e.g. python test.py)
    """

    screen_w, screen_h = get_screen_size()
    img_h, img_w, *_ = img.shape
    img_ratio = img_h / img_w

    window_w = round(screen_w / 4)
    window_h = round(window_w * img_ratio)

    resized = cv.resize(img, (window_w, window_h))
    
    cv.imshow(window_name, resized)
    cv.waitKey()

@only_if_env_variable('DEBUG_IMSHOW')
def _debug_imshow_multiple(window_name, images, cols, rows, w=400, h=400):
    fig = plt.figure()
    fig.suptitle(window_name)

    for i, (name, img) in enumerate(images.items()):
        subplot = fig.add_subplot(rows, cols, i + 1)
        subplot.title.set_text(name)
        plt.axis('off')

        is_grayscale = len(img.shape) == 2

        if is_grayscale:
          img = cv.cvtColor(img, cv.COLOR_GRAY2RGB)
        else:
          img = cv.cvtColor(img, cv.COLOR_BGR2RGB) # OpenCV default colorscheme is BGR

        plt.imshow(img) 
        
    plt.show()
    

def _debug_draw_contours(img, contours):
    draw_img = img.copy()
    if len(draw_img.shape) == 2: # Grayscale image
        draw_img = cv.cvtColor(draw_img, cv.COLOR_GRAY2BGR)

    cv.drawContours(draw_img, contours, -1, (255,255,0), 3)

    return draw_img

def _debug_show_contours(window_name, img, contours):
    """ Draw contours and display using __debug_imshow method """

    draw_img = _debug_draw_contours(img, contours)  
    
    _debug_imshow(window_name, draw_img)

@only_if_env_variable('DEBUG_PRINT')
def _debug_print(*args, **kw):
    print(*args, **kw)
