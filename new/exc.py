class MoleAnalyserError(Exception):
    pass 

class CalibrationError(MoleAnalyserError):
    def __init__(self, msg):
        super().__init__('Calibration failed: ' + msg)

class ReferencesDetectError(CalibrationError):
    def __init__(self, detected_count, required_count):
        super().__init__(f'not all references are detected ({detected_count} of {required_count})')

    

