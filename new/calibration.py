from math import pi as PI, cos, sin, sqrt
import os

import numpy as np
import cv2 as cv

from utils.morphology import imfill_holes
from utils.color import img_to_gray, img_mean_color
from utils.contours import get_equiv_diameter
from utils.debug import _debug_imshow, _debug_show_contours, _debug_imshow_multiple, _debug_draw_contours
from exc import ReferencesDetectError

REF_CIRCLE_R_MM = 2


def select_rgb_refs(refs_colors):
    """ Get red, green and blue colors by references """

    rgb_refs = {}

    for ch, ch_name in [(0, 'r'), (1, 'g'), (2, 'b')]:
        max_val = 0
        max_idx = 0

        for i in range(len(refs_colors)):
            if refs_colors[i][ch] > max_val:
                max_val = refs_colors[i][ch]
                max_idx = i

        rgb_refs[ch_name] = refs_colors[max_idx]

    return rgb_refs

def get_pixel_to_mm_rate(refs_radiuses):
    return np.mean(list(map(lambda x: REF_CIRCLE_R_MM/x, refs_radiuses))) 

def get_contour_radius(contour):
    equiv_d = get_equiv_diameter(contour)
    r = equiv_d / 2

    return r

def get_ref_params(img, color):
    assert color in ['red', 'green', 'blue'], f'Incorrect usage of the function (color={color})'

    if color == 'red':
        lowerH, upperH = 20, 200
    elif color == 'green':
        lowerH, upperH = 60, 120 
    elif color == 'blue':
        lowerH, upperH = 50, 160

    hsv = cv.cvtColor(img, cv.COLOR_BGR2HSV)
    mask = cv.inRange(hsv, (lowerH, 0, 0), (upperH, 250, 200))

    masked = cv.bitwise_and(img,img, mask=mask)

    h, w, _ = masked.shape

    pixels = []

    for y in range(h):
        for x in range(w):
            pixel = masked[y, x]

            if pixel.all():
                pixels.append(pixel)

    sum_b, sum_g, sum_r = 0, 0, 0

    for b, g, r in pixels:
        sum_b += b
        sum_g += g
        sum_r += r

    assert len(pixels) != 0, f'Color selection problem on the {color} dot image'

    avg_b = sum_b / len(pixels)
    avg_g = sum_g / len(pixels)
    avg_r = sum_r / len(pixels)

    diameter = sqrt(len(pixels))

    return (avg_r, avg_g, avg_b), diameter


def calibrate(calib_red_img, calib_green_img, calib_blue_img):
    """ Detecting reference colors and pix/mm rate """

    ref_r_color, ref_r_diameter = get_ref_params(calib_red_img, color='red')
    ref_g_color, ref_g_diameter = get_ref_params(calib_green_img, color='green')
    ref_b_color, ref_b_diameter = get_ref_params(calib_blue_img, color='blue')

    rgb_refs = {'r': ref_r_color, 'g': ref_g_color, 'b': ref_b_color }

    # Get pixel <-> mm convert rate
    pixel_to_mm_rate = get_pixel_to_mm_rate([ref_r_diameter/2, ref_g_diameter/2, ref_b_diameter/2])

    return rgb_refs, pixel_to_mm_rate
